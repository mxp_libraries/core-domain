<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order;

use Maxipost\CoreDomain\Order\ValueObject;

class Order
{
    private $id;
    private $serviceType;
    private $orderApishipId;
    private $additionalDeliveryServices;
    private $completedAdditionalDeliveryServices;
    private $places;
    private $dimensions;
    private $goods;
    private $serviceInfo;
    private $principal;
    private $deliverer;
    private $deliveryOrder;
    private $pickupOrder;
    private $sender;
    private $recipient;
    private $cashOnDelivery;
    private $paymentOnPrincipal;
    private $contracts;
    private $internalNumber;
    private $currentStatus;
    private $statuses;
    private $cashOnDeliveryStatus;

    /**
     * CreateOrder constructor.
     * @param ValueObject\ServiceType $serviceType
     * @param ValueObject\ApishipId $orderApishipId
     * @param ValueObject\Contracts $contracts
     * @param ValueObject\Principal $principal
     * @param ValueObject\Deliverer $deliverer
     * @param ValueObject\DeliveryOrder $deliveryOrder
     * @param ValueObject\PickupOrder $pickupOrder
     * @param ValueObject\Sender $sender
     * @param ValueObject\Recipient $recipient
     * @param ValueObject\CashOnDelivery $cashOnDelivery
     * @param ValueObject\PaymentOnPrincipal $paymentOnPrincipal
     * @param ValueObject\AdditionalDeliveryService[] $additionalDeliveryServices
     * @param ValueObject\CompletedAdditionalDeliveryService[] $completedAdditionalDeliveryServices
     * @param ValueObject\Place[] $places
     * @param ValueObject\Dimensions $dimensions
     * @param ValueObject\Goods[] $goods
     */
    public function __construct(
        ValueObject\ServiceType $serviceType,
        ValueObject\ApishipId $orderApishipId,
        ValueObject\Contracts $contracts,
        ValueObject\Principal $principal,
        ValueObject\Deliverer $deliverer,
        ValueObject\DeliveryOrder $deliveryOrder,
        ValueObject\PickupOrder $pickupOrder,
        ValueObject\Sender $sender,
        ValueObject\Recipient $recipient,
        ValueObject\CashOnDelivery $cashOnDelivery,
        ValueObject\PaymentOnPrincipal $paymentOnPrincipal,
        array $additionalDeliveryServices,
        array $completedAdditionalDeliveryServices,
        array $places,
        ValueObject\Dimensions $dimensions,
        array $goods
    )
    {
        $this->serviceType = $serviceType;
        $this->orderApishipId = $orderApishipId;
        $this->principal = $principal;
        $this->deliverer = $deliverer;
        $this->deliveryOrder = $deliveryOrder;
        $this->pickupOrder = $pickupOrder;
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->cashOnDelivery = $cashOnDelivery;
        $this->additionalDeliveryServices = $additionalDeliveryServices;
        $this->completedAdditionalDeliveryServices = $completedAdditionalDeliveryServices;
        $this->places = $places;
        $this->dimensions = $dimensions;
        $this->goods = $goods;
        $this->paymentOnPrincipal = $paymentOnPrincipal;
        $this->contracts = $contracts;
    }

    public function getId(): ValueObject\OrderId
    {
        return $this->id;
    }

    public function getServiceType(): ValueObject\ServiceType
    {
        return $this->serviceType;
    }

    public function getOrderApishipId(): ValueObject\ApishipId
    {
        return $this->orderApishipId;
    }

    /**
     * @return ValueObject\AdditionalDeliveryService[]
     */
    public function getAdditionalDeliveryServices(): array
    {
        return $this->additionalDeliveryServices;
    }

    /**
     * @return ValueObject\CompletedAdditionalDeliveryService[]
     */
    public function getCompletedAdditionalDeliveryServices(): array
    {
        return $this->completedAdditionalDeliveryServices;
    }

    /**
     * @return ValueObject\Place[]
     */
    public function getPlaces(): array
    {
        return $this->places;
    }

    public function getDimensions(): ValueObject\Dimensions
    {
        return $this->dimensions;
    }

    /**
     * @return ValueObject\Goods[]
     */
    public function getGoods(): array
    {
        return $this->goods;
    }

    public function getPaymentOnPrincipal(): ValueObject\PaymentOnPrincipal
    {
        return $this->paymentOnPrincipal;
    }

    public function getServiceInfo(): ValueObject\ServiceInfo
    {
        return $this->serviceInfo;
    }

    public function getPrincipal(): ValueObject\Principal
    {
        return $this->principal;
    }

    public function getDeliverer(): ValueObject\Deliverer
    {
        return $this->deliverer;
    }

    public function getDeliveryOrder(): ValueObject\DeliveryOrder
    {
        return $this->deliveryOrder;
    }

    public function getPickupOrder(): ValueObject\PickupOrder
    {
        return $this->pickupOrder;
    }

    public function getSender(): ValueObject\Sender
    {
        return $this->sender;
    }

    public function getRecipient(): ValueObject\Recipient
    {
        return $this->recipient;
    }

    public function getCashOnDelivery(): ValueObject\CashOnDelivery
    {
        return $this->cashOnDelivery;
    }

    public function getContracts(): ValueObject\Contracts
    {
        return $this->contracts;
    }

    public function getInternalNumber(): string
    {
        return $this->internalNumber;
    }

    /**
     * @return Status
     */
    public function getCurrentStatus(): Status
    {
        return $this->currentStatus;
    }

    /**
     * @return array|Status[]
     */
    public function getStatuses(): array
    {
        return $this->statuses;
    }

    public function getCashOnDeliveryStatus(): ValueObject\CashOnDeliveryStatus
    {
        return $this->cashOnDeliveryStatus;
    }
}
