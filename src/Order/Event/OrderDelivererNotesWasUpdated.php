<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\ValueObject;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderDelivererNotesWasUpdated extends DomainEvent
{
    private $delivererNote;

    public static function getEventId(): string
    {
        return 'order.deliverer.notes.wasUpdated';
    }

    public function __construct(
        ValueObject\OrderId $id,
        string $delivererNote
    ) {
        parent::__construct($id);
        $this->delivererNote = $delivererNote;
    }

    public function getDelivererNote(): string
    {
        return $this->delivererNote;
    }
}