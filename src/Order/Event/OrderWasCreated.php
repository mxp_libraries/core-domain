<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\ValueObject;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderWasCreated extends DomainEvent
{
    private $serviceInfo;
    private $serviceType;
    private $orderApishipId;
    private $principal;
    private $deliverer;
    private $deliveryOrder;
    private $pickupOrder;
    private $sender;
    private $recipient;
    private $cashOnDelivery;
    private $paymentOnPrincipal;
    private $paymentOnDelivery;
    private $additionalDeliveryServices;
    private $completedAdditionalDeliveryServices;
    private $places;
    private $dimensions;
    private $goods;
    private $internalNumber;
    private $contracts;
    private $statuses;
    private $cashOnDeliveryStatus;

    public static function getEventId(): string
    {
        return 'order.wasCreated';
    }

    public function __construct(
        ValueObject\OrderId $id,
        array $goods,
        ValueObject\ServiceInfo $serviceInfo,
        ValueObject\ServiceType $serviceType,
        string $internalNumber,
        ValueObject\ApishipId $orderApishipId,
        ValueObject\Contracts $contracts,
        ValueObject\Principal $principal,
        ValueObject\Deliverer $deliverer,
        ValueObject\DeliveryOrder $deliveryOrder,
        ValueObject\PickupOrder $pickupOrder,
        ValueObject\Sender $sender,
        ValueObject\Recipient $recipient,
        ValueObject\CashOnDelivery $cashOnDelivery,
        ValueObject\PaymentOnPrincipal $paymentOnPrincipal,
        ValueObject\PaymentOnDelivery $paymentOnDelivery,
        array $additionalDeliveryServices,
        array $completedAdditionalDeliveryServices,
        array $places,
        ValueObject\Dimensions $dimensions,
        array $statuses,
        ValueObject\CashOnDeliveryStatus $cashOnDeliveryStatus
    ) {
        parent::__construct($id);
        $this->serviceInfo = $serviceInfo;
        $this->serviceType = $serviceType;
        $this->orderApishipId = $orderApishipId;
        $this->principal = $principal;
        $this->deliverer = $deliverer;
        $this->deliveryOrder = $deliveryOrder;
        $this->pickupOrder = $pickupOrder;
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->cashOnDelivery = $cashOnDelivery;
        $this->paymentOnPrincipal = $paymentOnPrincipal;
        $this->paymentOnDelivery = $paymentOnDelivery;
        $this->additionalDeliveryServices = $additionalDeliveryServices;
        $this->completedAdditionalDeliveryServices = $completedAdditionalDeliveryServices;
        $this->places = $places;
        $this->dimensions = $dimensions;
        $this->goods = $goods;
        $this->internalNumber = $internalNumber;
        $this->contracts = $contracts;
        $this->statuses = $statuses;
        $this->cashOnDeliveryStatus = $cashOnDeliveryStatus;
    }

    public function getContracts(): ValueObject\Contracts
    {
        return $this->contracts;
    }

    public function getInternalNumber(): string
    {
        return $this->internalNumber;
    }

    public function getServiceInfo(): ValueObject\ServiceInfo
    {
        return $this->serviceInfo;
    }

    public function getServiceType(): ValueObject\ServiceType
    {
        return $this->serviceType;
    }

    public function getOrderApishipId(): ValueObject\ApishipId
    {
        return $this->orderApishipId;
    }

    public function getPrincipal(): ValueObject\Principal
    {
        return $this->principal;
    }

    public function getDeliverer(): ValueObject\Deliverer
    {
        return $this->deliverer;
    }

    public function getDeliveryOrder(): ValueObject\DeliveryOrder
    {
        return $this->deliveryOrder;
    }

    public function getPickupOrder(): ValueObject\PickupOrder
    {
        return $this->pickupOrder;
    }

    public function getSender(): ValueObject\Sender
    {
        return $this->sender;
    }

    public function getRecipient(): ValueObject\Recipient
    {
        return $this->recipient;
    }

    public function getCashOnDelivery(): ValueObject\CashOnDelivery
    {
        return $this->cashOnDelivery;
    }

    public function getPaymentOnPrincipal(): ValueObject\PaymentOnPrincipal
    {
        return $this->paymentOnPrincipal;
    }

    public function getPaymentOnDelivery(): ValueObject\PaymentOnDelivery
    {
        return $this->paymentOnDelivery;
    }

    public function getAdditionalDeliveryServices(): array
    {
        return $this->additionalDeliveryServices;
    }

    public function getCompletedAdditionalDeliveryServices(): array
    {
        return $this->completedAdditionalDeliveryServices;
    }

    public function getPlaces(): array
    {
        return $this->places;
    }

    public function getDimensions(): ValueObject\Dimensions
    {
        return $this->dimensions;
    }

    public function getGoods(): array
    {
        return $this->goods;
    }

    public function getStatuses(): array
    {
        return $this->statuses;
    }

    public function getCashOnDeliveryStatus(): ValueObject\CashOnDeliveryStatus
    {
        return $this->cashOnDeliveryStatus;
    }

}
