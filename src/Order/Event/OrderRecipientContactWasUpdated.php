<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderRecipientContactWasUpdated extends DomainEvent
{
    private $name;
    private $phone;
    private $email;

    public static function getEventId(): string
    {
        return 'order.recipientContact.wasUpdated';
    }

    public function __construct
    (
        OrderId $id,
        string $name,
        string $phone,
        ?string $email
    ) {
        parent::__construct($id);
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}