<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\DeliveryZone\ValueObject\DeliveryZoneId;
use Maxipost\CoreDomain\Order\ValueObject;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderServiceInfoDeliveryZoneIdWasUpdated extends DomainEvent
{
    private $deliveryZoneId;

    public static function getEventId(): string
    {
        return 'order.serviceInfo.deliveryZoneId.wasUpdated';
    }

    public function __construct(
        ValueObject\OrderId $id,
        DeliveryZoneId $deliveryZoneId
    ) {
        parent::__construct($id);
        $this->deliveryZoneId = $deliveryZoneId;
    }

    public function getDeliveryZoneId(): DeliveryZoneId
    {
        return $this->deliveryZoneId;
    }
}