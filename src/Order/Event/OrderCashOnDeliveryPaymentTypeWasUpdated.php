<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\ValueObject;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderCashOnDeliveryPaymentTypeWasUpdated extends DomainEvent
{
    private $paymentType;

    public static function getEventId(): string
    {
        return 'order.cashOnDelivery.paymentType.wasUpdated';
    }

    public function __construct(
        ValueObject\OrderId $id,
        ValueObject\CashOnDelivery\PaymentType $paymentType
    ) {
        parent::__construct($id);
        $this->paymentType = $paymentType;
    }

    public function getPaymentType(): ValueObject\CashOnDelivery\PaymentType
    {
        return $this->paymentType;
    }
}