<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Common\ValueObject\Date;
use Maxipost\CoreDomain\Common\ValueObject\TimeInterval;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderDeliveryDateWasUpdated extends DomainEvent
{
    private $date;
    private $timeInterval;

    public static function getEventId(): string
    {
        return 'order.deliveryOrder.dateTimeInterval.wasUpdated';
    }

    public function getDate(): ?Date
    {
        return $this->date;
    }

    public function getTimeInterval(): ?TimeInterval
    {
        return $this->timeInterval;
    }

    public function __construct
    (
        OrderId $id,
        ?Date $date,
        ?TimeInterval $timeInterval
    ) {
        parent::__construct($id);
        $this->date = $date;
        $this->timeInterval = $timeInterval;
    }
}