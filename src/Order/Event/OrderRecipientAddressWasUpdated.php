<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderRecipientAddressWasUpdated extends DomainEvent
{
    private $value;
    private $unrestricted_value;
    private $string;
    private $country;
    private $city;
    private $street;
    private $house;
    private $block;
    private $office;
    private $postalCode;
    private $fias;
    private $data;
    private $longitude;
    private $latitude;

    public static function getEventId(): string
    {
        return 'order.recipientAddress.wasUpdated';
    }

    public function __construct
    (
        OrderId $id,
        string $unrestricted_value,
        ?string $value = null,
        ?string $string = null,
        ?string $country = null,
        ?string $city = null,
        ?string $street = null,
        ?string $house = null,
        ?string $block = null,
        ?string $office = null,
        ?string $postalCode = null,
        ?string $fias = null,
        ?string $longitude = null,
        ?string $latitude = null,
        array $data = []
    ) {
        parent::__construct($id);
        $this->unrestricted_value = $unrestricted_value;
        $this->value = $value;
        $this->string = $string;
        $this->country = $country;
        $this->city = $city;
        $this->street = $street;
        $this->house = $house;
        $this->block = $block;
        $this->office = $office;
        $this->postalCode = $postalCode;
        $this->fias = $fias;
        $this->data = $data;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function getUnrestrictedValue(): string
    {
        return $this->unrestricted_value;
    }

    public function getString(): ?string
    {
        return $this->string;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function getHouse(): ?string
    {
        return $this->house;
    }

    public function getBlock(): ?string
    {
        return $this->block;
    }

    public function getOffice(): ?string
    {
        return $this->office;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function getFias(): ?string
    {
        return $this->fias;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getData(): array
    {
        return $this->data;
    }
}