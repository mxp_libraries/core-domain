<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\Status;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderStatusWasChanged extends DomainEvent
{
    private $status;

    public static function getEventId(): string
    {
        return 'order.status.wasChanged';
    }

    public function __construct(
        OrderId $id,
        Status $status
    ) {
        parent::__construct($id);
        $this->status = $status;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }
}