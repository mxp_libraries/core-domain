<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderDimensionsWasUpdated extends DomainEvent
{
    private $weight;
    private $weightFact;
    private $height;
    private $heightFact;
    private $length;
    private $lengthFact;
    private $width;
    private $widthFact;
    private $planVolumeWeight;
    private $factVolumeWeight;

    public static function getEventId(): string
    {
        return 'order.dimensions.wasUpdated';
    }

    public function __construct(
        OrderId $id,
        ?int $weight = 0,
        ?int $weightFact = 0,
        ?int $height = 0,
        ?int $heightFact = 0,
        ?int $length = 0,
        ?int $lengthFact = 0,
        ?int $width = 0,
        ?int $widthFact = 0,
        ?int $planVolumeWeight = 0,
        ?int $factVolumeWeight = 0
    ) {
        parent::__construct($id);
        $this->weight = $weight;
        $this->weightFact = $weightFact;
        $this->height = $height;
        $this->heightFact = $heightFact;
        $this->length = $length;
        $this->lengthFact = $lengthFact;
        $this->width = $width;
        $this->widthFact = $widthFact;
        $this->planVolumeWeight = $planVolumeWeight;
        $this->factVolumeWeight = $factVolumeWeight;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function getWeightFact(): ?int
    {
        return $this->weightFact;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getHeightFact(): ?int
    {
        return $this->heightFact;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function getLengthFact(): ?int
    {
        return $this->lengthFact;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getWidthFact(): ?int
    {
        return $this->widthFact;
    }

    public function getPlanVolumeWeight(): ?int
    {
        return $this->planVolumeWeight;
    }

    public function getFactVolumeWeight(): ?int
    {
        return $this->factVolumeWeight;
    }
}
