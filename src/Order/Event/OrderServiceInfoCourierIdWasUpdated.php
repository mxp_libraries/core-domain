<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\Order\ValueObject;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderServiceInfoCourierIdWasUpdated extends DomainEvent
{
    private $courierId;

    public static function getEventId(): string
    {
        return 'order.serviceInfo.courierId.wasUpdated';
    }

    public function __construct(
        ValueObject\OrderId $id,
        CourierId $courierId
    ) {
        parent::__construct($id);
        $this->courierId = $courierId;
    }

    public function getCourierId(): CourierId
    {
        return $this->courierId;
    }
}