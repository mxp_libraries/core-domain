<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\ValueObject;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\OrderServicePointId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderOrderServicePointIdWasUpdated extends DomainEvent
{
    private $orderServicePointId;

    public static function getEventId(): string
    {
        return 'order.orderDelivery.orderServicePointId.wasUpdated';
    }

    public function __construct(
        ValueObject\OrderId $id,
        OrderServicePointId $orderServicePointId
    ) {
        parent::__construct($id);
        $this->orderServicePointId = $orderServicePointId;
    }

    public function getOrderServicePointId(): OrderServicePointId
    {
        return $this->orderServicePointId;
    }
}