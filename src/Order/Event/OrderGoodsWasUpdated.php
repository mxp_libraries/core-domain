<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\Event;

use Maxipost\CoreDomain\Order\ValueObject\Goods;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderGoodsWasUpdated extends DomainEvent
{
    private $goods;

    public static function getEventId(): string
    {
        return 'order.goods.wasUpdated';
    }

    /**
     * @return Goods[]
     */
    public function getGoods(): array
    {
        return $this->goods;
    }

    /**
     * OrderGoodsWasUpdated constructor.
     * @param OrderId $id
     * @param Goods[] $goods
     */
    public function __construct(OrderId $id, array $goods)
    {
        parent::__construct($id);
        $this->goods = $goods;
    }
}