<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order;

use DateTimeImmutable;
use Maxipost\CoreDomain\Order\ValueObject\CashOnDelivery\PaymentType;
use Maxipost\CoreDomain\Order\ValueObject\OrderStatusId;
use Maxipost\CoreDomain\OrderStatusModel\ValueObject\OrderStatusModelId;

class Status
{
    public const DEFAULT_STATUS_MODEL_ID = self::STATUS_MODEL_ID_CREATED;
    public const STATUS_MODEL_ID_CREATED = '102000';
    public const STATUS_MODEL_ID_PARTIALLY_DELIVERED = '419000';
    public const STATUS_MODEL_ID_DELIVERED = '417000';
    public const STATUS_MODEL_ID_DELIVERY_FAILURED = '420207';
    public const STATUS_MODEL_ID_FAILURE_WITHOUT_DELIVERY = '420206';
    private $_id;
    private $orderStatusModelId;
    private $sourceCreatedAt;
    private $coreCreatedAt;
    private $comment;
    private $paymentType;
    private $name;
    /**
     * @var \Maxipost\CoreDomain\Order\ValueObject\Status\Goods[]
     */
    private $goods;

    public function __construct(
        OrderStatusId $id,
        OrderStatusModelId $orderStatusModelId,
        DateTimeImmutable $sourceCreatedAt = null,
        string $comment = '',
        PaymentType $paymentType = null,
        array $goods = []
    )
    {
        $this->_id = $id;
        $this->orderStatusModelId = $orderStatusModelId;
        $this->comment = $comment;
        $this->coreCreatedAt = new DateTimeImmutable();
        $this->sourceCreatedAt = $sourceCreatedAt ?? $this->coreCreatedAt;
        $this->paymentType = $paymentType;
        $this->goods = $goods;
    }

    public function getId(): OrderStatusId
    {
        return $this->_id;
    }

    public function getOrderStatusModelId(): OrderStatusModelId
    {
        return $this->orderStatusModelId;
    }

    public function getSourceCreatedAt(): DateTimeImmutable
    {
        return $this->sourceCreatedAt;
    }

    public function getCoreCreatedAt(): DateTimeImmutable
    {
        return $this->coreCreatedAt;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function getPaymentType(): PaymentType
    {
        return $this->paymentType;
    }

    /**
     * @return \Maxipost\CoreDomain\Order\ValueObject\Status\Goods[]
     */
    public function getGoods(): array
    {
        return $this->goods;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
