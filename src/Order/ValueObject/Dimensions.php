<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

class Dimensions
{
    public const VOLUME_WEIGHT_RATIO_5 = 5;
    private $weight;
    private $weightFact;
    private $height;
    private $heightFact;
    private $length;
    private $lengthFact;
    private $width;
    private $widthFact;
    private $planVolumeWeight;
    private $factVolumeWeight;

    public function __construct(
        ?int $weight = 0,
        ?int $weightFact = 0,
        ?int $height = 0,
        ?int $heightFact = 0,
        ?int $length = 0,
        ?int $lengthFact = 0,
        ?int $width = 0,
        ?int $widthFact = 0,
        ?int $planVolumeWeight = 0,
        ?int $factVolumeWeight = 0
    ) {
        $this->weight = $weight;
        $this->weightFact = $weightFact;
        $this->height = $height;
        $this->heightFact = $heightFact;
        $this->length = $length;
        $this->lengthFact = $lengthFact;
        $this->width = $width;
        $this->widthFact = $widthFact;
        $this->planVolumeWeight = $planVolumeWeight;
        $this->factVolumeWeight = $factVolumeWeight;
    }

    public function calculatePlanVolumeWeight(): float
    {
        return ceil($this->height * $this->width * $this->length / self::VOLUME_WEIGHT_RATIO_5);
    }

    public function calculateFactVolumeWeight(): float
    {
        return ceil($this->heightFact * $this->widthFact * $this->lengthFact / self::VOLUME_WEIGHT_RATIO_5);
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function getWeightFact(): ?int
    {
        return $this->weightFact;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getHeightFact(): ?int
    {
        return $this->heightFact;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function getLengthFact(): ?int
    {
        return $this->lengthFact;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getWidthFact(): ?int
    {
        return $this->widthFact;
    }

    public function getPlanVolumeWeight(): ?int
    {
        return $this->planVolumeWeight;
    }

    public function getFactVolumeWeight(): ?int
    {
        return $this->factVolumeWeight;
    }
}
