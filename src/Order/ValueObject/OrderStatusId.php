<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class OrderStatusId extends AggregateRootId
{
}