<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\DateTimeIntervalSimple;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\OrderServicePointId;

class DeliveryOrder
{
    private $orderServicePointId;
    private $dateTimeInterval;

    public function __construct(
        DateTimeIntervalSimple $dateTimeInterval,
        ?OrderServicePointId $orderServicePointId
    ) {
        $this->orderServicePointId = $orderServicePointId;
        $this->dateTimeInterval = $dateTimeInterval;
    }

    public function getOrderServicePointId(): ?OrderServicePointId
    {
        return $this->orderServicePointId;
    }

    public function getDateTimeInterval(): DateTimeIntervalSimple
    {
        return $this->dateTimeInterval?? new DateTimeIntervalSimple();
    }
}
