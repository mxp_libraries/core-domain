<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

class PaymentOnPrincipal
{
    private $clientPaymentDate;
    private $clientStatutoryPaymentDate;
    private $clientPaymentReceiptNumber;

    public function __construct(
        ?string $clientPaymentDate = null,
        ?string $clientStatutoryPaymentDate = null,
        ?string $clientPaymentReceiptNumber = null
    ) {
        $this->clientPaymentDate = $clientPaymentDate;
        $this->clientStatutoryPaymentDate = $clientStatutoryPaymentDate;
        $this->clientPaymentReceiptNumber = $clientPaymentReceiptNumber;
    }

    public function getClientPaymentDate(): ?string
    {
        return $this->clientPaymentDate;
    }

    public function getClientStatutoryPaymentDate(): ?string
    {
        return $this->clientStatutoryPaymentDate;
    }

    public function getClientPaymentReceiptNumber(): ?string
    {
        return $this->clientPaymentReceiptNumber;
    }
}
