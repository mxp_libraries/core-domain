<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

class CashOnDelivery
{
    private $paymentType;
    private $priceDeclared;
    private $priceFact;
    private $estimatedCost;

    public function __construct(
        ?CashOnDelivery\PaymentType $paymentType = null,
        ?int $priceDeclared = 0,
        ?int $priceFact = 0,
        ?int $estimatedCost = 0
    ) {
        $this->paymentType = $paymentType;
        $this->priceDeclared = $priceDeclared;
        $this->priceFact = $priceFact;
        $this->estimatedCost = $estimatedCost;
    }

    public function getPaymentType(): ?CashOnDelivery\PaymentType
    {
        return $this->paymentType;
    }

    public function getPriceDeclared(): ?int
    {
        return $this->priceDeclared;
    }

    public function getPriceFact(): ?int
    {
        return $this->priceFact;
    }

    public function getEstimatedCost(): ?int
    {
        return $this->estimatedCost;
    }
}
