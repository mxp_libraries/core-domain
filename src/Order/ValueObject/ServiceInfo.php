<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use DateTimeImmutable;
use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\DeliveryZone\ValueObject\DeliveryZoneId;

class ServiceInfo
{
    private $deliveryZoneId;
    private $courierId;
    private $createdAt;

    public function __construct(
        ?DeliveryZoneId $deliveryZoneId,
        ?CourierId $courierId,
        ?DateTimeImmutable $createdAt
    ) {
        $this->deliveryZoneId = $deliveryZoneId;
        $this->courierId = $courierId;
        $this->createdAt = $createdAt;
    }

    public function getDeliveryZoneId(): ?DeliveryZoneId
    {
        return $this->deliveryZoneId;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCourierId(): ?CourierId
    {
        return $this->courierId;
    }
}
