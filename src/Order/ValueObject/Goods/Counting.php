<?php

namespace Maxipost\CoreDomain\Order\ValueObject\Goods;

class Counting
{
    private $count;
    private $countFact;
    private $countDelivered;
    private $countNotDelivered;
    private $countLost;
    private $countMissing;

    public function __construct(
        int $count = 1,
        ?int $countFact = 0,
        ?int $countDelivered = 0,
        ?int $countNotDelivered = 0,
        ?int $countLost = 0,
        ?int $countMissing = 0
    ) {
        $this->count = $count;
        $this->countFact = $countFact;
        $this->countDelivered = $countDelivered;
        $this->countNotDelivered = $countNotDelivered;
        $this->countLost = $countLost;
        $this->countMissing = $countMissing;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function getCountFact(): ?int
    {
        return $this->countFact;
    }

    public function getCountDelivered(): ?int
    {
        return $this->countDelivered;
    }

    public function getCountNotDelivered(): ?int
    {
        return $this->countNotDelivered;
    }

    public function getCountLost(): ?int
    {
        return $this->countLost;
    }

    public function getCountMissing(): ?int
    {
        return $this->countMissing;
    }
}
