<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject\Goods;

use Maxipost\DomainEventSourcing\AggregateRootId;

class ItemId extends AggregateRootId
{
}