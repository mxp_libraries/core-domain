<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Assert\Assertion;

class ServiceType
{
    public const SELF_PICKUP = 1;
    public const TYPES = [
        0 => 'Курьерская',
        self::SELF_PICKUP => 'Самовывоз',
        2 => 'Почта',
        3 => 'Дрон',
    ];
    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}
