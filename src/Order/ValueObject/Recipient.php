<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\Address;

class Recipient
{
    private $address;
    private $contacts;
    private $notes;

    public function __construct(
        Address $address,
        Recipient\Contacts $contacts,
        string $notes = ''
    ) {
        $this->address = $address;
        $this->contacts = $contacts;
        $this->notes = $notes;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function getContacts(): Recipient\Contacts
    {
        return $this->contacts;
    }

    public function getNotes(): string
    {
        return $this->notes ?? '';
    }
}
