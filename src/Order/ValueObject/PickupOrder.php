<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\DateTimeIntervalSimple;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;

class PickupOrder
{
    private $pickupType;
    private $warehouseId;
    private $dateTimeInterval;

    public function __construct(
        ?PickupOrder\Type $pickupType = null,
        ?WarehouseId $warehouseId = null,
        ?DateTimeIntervalSimple $dateTimeInterval = null
    ) {
        $this->pickupType = $pickupType;
        $this->warehouseId = $warehouseId;
        $this->dateTimeInterval = $dateTimeInterval;
    }

    public function getPickupType(): ?PickupOrder\Type
    {
        return $this->pickupType;
    }

    public function getWarehouseId(): ?WarehouseId
    {
        return $this->warehouseId;
    }

    public function getDateTimeInterval(): ?DateTimeIntervalSimple
    {
        return $this->dateTimeInterval;
    }
}
