<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

class Place
{
    private $barcode;
    private $placeNum;
    private $dimensions;
    private $internalBarcode;

    public function __construct(
        string $barcode,
        int $placeNum,
        ?string $internalBarcode = null,
        ?Dimensions $dimensions = null
    ) {
        $this->barcode = $barcode;
        $this->placeNum = $placeNum;
        $this->dimensions = $dimensions;
        $this->internalBarcode = $internalBarcode;
    }

    public function getBarcode(): string
    {
        return $this->barcode;
    }

    public function getPlaceNum(): int
    {
        return $this->placeNum;
    }

    public function getDimensions(): ?Dimensions
    {
        return $this->dimensions;
    }

    public function getInternalBarcode(): ?string
    {
        return $this->internalBarcode;
    }
}
