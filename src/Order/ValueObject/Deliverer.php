<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;

class Deliverer
{
    private $internalNumber;
    private $legalPersonId;
    private $warehouseId;
    private $notes;

    public function __construct(
        ?string $internalNumber = null,
        ?LegalPersonId $legalPersonId = null,
        ?WarehouseId $warehouseId = null,
        ?string $notes = null
    ) {
        $this->internalNumber = $internalNumber;
        $this->legalPersonId = $legalPersonId;
        $this->warehouseId = $warehouseId;
        $this->notes = $notes;
    }

    public function getInternalNumber(): ?string
    {
        return $this->internalNumber;
    }

    public function getLegalPersonId(): ?LegalPersonId
    {
        return $this->legalPersonId;
    }

    public function getWarehouseId(): ?WarehouseId
    {
        return $this->warehouseId;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }
}
