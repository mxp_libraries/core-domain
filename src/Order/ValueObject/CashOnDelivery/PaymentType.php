<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject\CashOnDelivery;

use Assert\Assertion;

class PaymentType
{
    public const PREPAYMENT = 0;
    public const CASH = 1;
    public const CARD = 2;
    public const SAFE_DEAL = 3;
    public const TYPES = [
        self::PREPAYMENT => 'Предоплата',
        self::CASH => 'Наличными',
        self::CARD => 'Картой',
        self::SAFE_DEAL => 'Безопасная сделка',
    ];
    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}
