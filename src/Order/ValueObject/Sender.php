<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;

class Sender
{
    private $internalNumber;
    private $providerNumber;
    private $brandName;
    private $corporationName;
    private $legalPersonId;
    private $warehouseId;

    public function __construct(
        string $internalNumber = null,
        ?string $providerNumber = null,
        ?string $brandName = null,
        ?string $corporationName = null,
        ?LegalPersonId $legalPersonId = null,
        ?WarehouseId $warehouseId = null
    ) {
        $this->internalNumber = $internalNumber;
        $this->providerNumber = $providerNumber;
        $this->brandName = $brandName;
        $this->corporationName = $corporationName;
        $this->legalPersonId = $legalPersonId;
        $this->warehouseId = $warehouseId;
    }

    public function getInternalNumber(): string
    {
        return $this->internalNumber;
    }

    public function getProviderNumber(): ?string
    {
        return $this->providerNumber;
    }

    public function getBrandName(): ?string
    {
        return $this->brandName;
    }

    public function getCorporationName(): ?string
    {
        return $this->corporationName;
    }

    public function getLegalPersonId(): ?LegalPersonId
    {
        return $this->legalPersonId;
    }

    public function getWarehouseId(): ?WarehouseId
    {
        return $this->warehouseId;
    }
}
