<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class OrderId extends AggregateRootId
{
}
