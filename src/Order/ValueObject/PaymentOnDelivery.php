<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

class PaymentOnDelivery
{
    private $principalPaymentDate;
    private $principalStatutoryPaymentDate;
    private $principalPaymentReceiptNumber;

    public function __construct(
        ?string $principalPaymentDate = null,
        ?string $principalStatutoryPaymentDate = null,
        ?string $principalPaymentReceiptNumber = null
    ) {
        $this->principalPaymentDate = $principalPaymentDate;
        $this->principalStatutoryPaymentDate = $principalStatutoryPaymentDate;
        $this->principalPaymentReceiptNumber = $principalPaymentReceiptNumber;
    }

    public function getPrincipalPaymentDate(): ?string
    {
        return $this->principalPaymentDate;
    }

    public function getPrincipalStatutoryPaymentDate(): ?string
    {
        return $this->principalStatutoryPaymentDate;
    }

    public function getPrincipalPaymentReceiptNumber(): ?string
    {
        return $this->principalPaymentReceiptNumber;
    }
}
