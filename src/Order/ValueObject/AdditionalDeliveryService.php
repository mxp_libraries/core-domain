<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\Tax;
use Maxipost\CoreDomain\DeliveryService\ValueObject\DeliveryServiceCode;
use Maxipost\CoreDomain\Order\ValueObject\AdditionalDeliveryService\ServicePayer;

class AdditionalDeliveryService
{
    private $deliveryServiceId;
    private $count;
    private $price;
    private $tax;
    private $servicePayer;

    public function __construct(
        ?DeliveryServiceCode $deliveryServiceId = null,
        ?int $count = null,
        ?int $price = null,
        ?Tax $tax = null,
        ?ServicePayer $servicePayer = null
    ) {
        $this->deliveryServiceId = $deliveryServiceId;
        $this->count = $count;
        $this->price = $price;
        $this->tax = $tax;
        $this->servicePayer = $servicePayer;
    }

    public function getDeliveryServiceId(): ?DeliveryServiceCode
    {
        return $this->deliveryServiceId;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function getTax(): ?Tax
    {
        return $this->tax;
    }

    public function getServicePayer(): ?ServicePayer
    {
        return $this->servicePayer;
    }
}
