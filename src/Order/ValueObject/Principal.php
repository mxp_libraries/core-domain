<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\Contract\ValueObject\ContractId;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;

class Principal
{
    private $internalNumber;
    private $legalPersonId;
    private $warehouseId;
    private $contractId;

    public function __construct(
        ?string $internalNumber = null,
        ?LegalPersonId $legalPersonId = null,
        ?WarehouseId $warehouseId = null,
        ?ContractId $contractId = null
    ) {
        $this->internalNumber = $internalNumber;
        $this->legalPersonId = $legalPersonId;
        $this->warehouseId = $warehouseId;
        $this->contractId = $contractId;
    }

    public function getInternalNumber(): ?string
    {
        return $this->internalNumber;
    }

    public function getLegalPersonId(): ?LegalPersonId
    {
        return $this->legalPersonId;
    }

    public function getWarehouseId(): ?WarehouseId
    {
        return $this->warehouseId;
    }

    public function getContractId(): ?ContractId
    {
        return $this->contractId;
    }
}
