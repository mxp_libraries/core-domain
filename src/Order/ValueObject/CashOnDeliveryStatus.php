<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;


use Assert\Assertion;

class CashOnDeliveryStatus
{
    public const UNPAID = 0;
    public const PAID = 1;
    public const COURIER_DELIVERY_REPORT = 2;
    public const RETRIEVED_FROM_DELIVERER = 3;
    public const TRANSFERRED_TO_SENDER = 4;

    public const TYPES = [
        self::UNPAID => ['id' => self::UNPAID, 'name' => 'Заказ не оплачен'],
        self::PAID => ['id' => self::PAID, 'name' => 'Заказ оплачен'],
        self::COURIER_DELIVERY_REPORT => ['id' => self::COURIER_DELIVERY_REPORT, 'name' => 'Курьер отчитался о Доставке'],
        self::RETRIEVED_FROM_DELIVERER => ['id' => self::RETRIEVED_FROM_DELIVERER, 'name' => 'Получено от Доставщика'],
        self::TRANSFERRED_TO_SENDER => ['id' => self::TRANSFERRED_TO_SENDER, 'name' => 'Переведено Отправителю'],
    ];

    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}