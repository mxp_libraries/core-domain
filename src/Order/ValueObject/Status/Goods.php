<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject\Status;

class Goods
{
    private $itemId;
    private $count;

    public function __construct(
        string $itemId,
        int $count
    ) {
        $this->itemId = $itemId;
        $this->count = $count;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}