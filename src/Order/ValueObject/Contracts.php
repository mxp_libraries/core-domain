<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\Contract\ValueObject\ContractId;

class Contracts
{
    private $senderPrincipalContractId;
    private $principalDelivererContractId;

    public function __construct(
        ?ContractId $senderPrincipalContractId = null,
        ?ContractId $principalDelivererContractId = null
    ) {
        $this->senderPrincipalContractId = $senderPrincipalContractId;
        $this->principalDelivererContractId = $principalDelivererContractId;
    }

    public function getSenderPrincipalContractId(): ?ContractId
    {
        return $this->senderPrincipalContractId;
    }

    public function getPrincipalDelivererContractId(): ?ContractId
    {
        return $this->principalDelivererContractId;
    }
}
