<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\Date;
use Maxipost\CoreDomain\DeliveryService\ValueObject\DeliveryServiceCode;

class CompletedAdditionalDeliveryService
{
    private $deliveryServiceId;
    private $serviceDate;
    private $cost;
    private $count;

    public function __construct(
        ?DeliveryServiceCode $deliveryServiceId = null,
        ?Date $serviceDate = null,
        ?int $cost = null,
        ?int $count = null
    ) {
        $this->deliveryServiceId = $deliveryServiceId;
        $this->serviceDate = $serviceDate;
        $this->cost = $cost;
        $this->count = $count;
    }

    public function getDeliveryServiceId(): ?DeliveryServiceCode
    {
        return $this->deliveryServiceId;
    }

    public function getServiceDate(): ?Date
    {
        return $this->serviceDate;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }
}
