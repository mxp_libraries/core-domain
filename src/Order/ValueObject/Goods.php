<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Order\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\Tax;
use Maxipost\CoreDomain\Order\ValueObject\Goods\Counting;
use Maxipost\CoreDomain\Order\ValueObject\Goods\ItemId;

class Goods
{
    private $vendorCode;
    private $code;
    private $name;
    private $barcode;
    private $counting;
    private $price;
    private $estimatedCost;
    private $tax;
    private $itemId;

    public function __construct(
        string $vendorCode,
        string $name,
        Counting $counting,
        int $price,
        Tax $tax,
        ?int $estimatedCost = null,
        ?string $code = null,
        ?string $barcode = null,
        ?ItemId $itemId = null
    ) {
        $this->itemId = $itemId;
        $this->vendorCode = $vendorCode;
        $this->code = $code;
        $this->name = $name;
        $this->barcode = $barcode;
        $this->counting = $counting;
        $this->price = $price;
        $this->estimatedCost = $estimatedCost;
        $this->tax = $tax;
    }

    public function getVendorCode(): string
    {
        return $this->vendorCode;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCounting(): Counting
    {
        return $this->counting;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getTax(): Tax
    {
        return $this->tax;
    }

    public function getEstimatedCost(): ?int
    {
        return $this->estimatedCost;
    }

    public function getItemId(): ?ItemId
    {
        return $this->itemId;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }
}
