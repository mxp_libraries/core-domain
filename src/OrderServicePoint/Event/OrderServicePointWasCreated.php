<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderServicePoint\Event;

use Maxipost\CoreDomain\Common\ValueObject\Address;
use Maxipost\CoreDomain\Common\ValueObject\InternalCode;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\ApishipId;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\Parameters;
use Maxipost\DomainEventSourcing\AggregateRootId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderServicePointWasCreated extends DomainEvent
{
    private $orderServicePointApishipId;
    private $counterpartyId;
    private $internalCode;
    private $isInternal;
    private $providerCode;
    private $type;
    private $name;
    private $description;
    private $isPickupAccept;
    private $isAcceptanceAccept;
    private $isFittingAccept;
    private $isSafeStorage;
    private $isCashAccept;
    private $isCardAccept;
    private $isActual;
    private $brandingType;
    private $contacts;
    private $operatingTime;
    private $parameters;
    private $address;
    private $photos;

    public static function getEventId(): string
    {
        return 'orderServicePoint.wasCreated';
    }

    public function __construct(
        AggregateRootId $id,
        InternalCode $internalCode,
        CounterpartyId $counterpartyId,
        string $name,
        int $type = null,
        ?string $providerCode = null,
        ?bool $isInternal = null,
        ?ApishipId $orderServicePointApishipId = null,
        ?string $description = null,
        ?bool $isPickupAccept = null,
        ?bool $isAcceptanceAccept = null,
        ?bool $isFittingAccept = null,
        ?bool $isSafeStorage = null,
        ?bool $isCashAccept = null,
        ?bool $isCardAccept = null,
        ?bool $isActual = null,
        ?int $brandingType = null,
        ?array $contacts = null,
        ?array $operatingTime = null,
        ?Address $address = null,
        ?Parameters $parameters = null,
        ?array $photos = null
    ) {
        parent::__construct($id);
        $this->orderServicePointApishipId = $orderServicePointApishipId;
        $this->counterpartyId = $counterpartyId;
        $this->internalCode = $internalCode;
        $this->isInternal = $isInternal;
        $this->providerCode = $providerCode;
        $this->type = $type;
        $this->name = $name;
        $this->description = $description;
        $this->isPickupAccept = $isPickupAccept;
        $this->isAcceptanceAccept = $isAcceptanceAccept;
        $this->isFittingAccept = $isFittingAccept;
        $this->isSafeStorage = $isSafeStorage;
        $this->isCashAccept = $isCashAccept;
        $this->isCardAccept = $isCardAccept;
        $this->isActual = $isActual;
        $this->brandingType = $brandingType;
        $this->contacts = $contacts;
        $this->operatingTime = $operatingTime;
        $this->address = $address;
        $this->parameters = $parameters;
        $this->photos = $photos;
    }

    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function getInternalCode(): InternalCode
    {
        return $this->internalCode;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOrderServicePointApishipId(): ?ApishipId
    {
        return $this->orderServicePointApishipId;
    }

    public function getProviderCode(): ?string
    {
        return $this->providerCode;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function isPickupAccept(): ?bool
    {
        return $this->isPickupAccept;
    }

    public function isAcceptanceAccept(): ?bool
    {
        return $this->isAcceptanceAccept;
    }

    public function isFittingAccept(): ?bool
    {
        return $this->isFittingAccept;
    }

    public function isSafeStorage(): ?bool
    {
        return $this->isSafeStorage;
    }

    public function isCashAccept(): ?bool
    {
        return $this->isCashAccept;
    }

    public function isCardAccept(): ?bool
    {
        return $this->isCardAccept;
    }

    public function isActual(): ?bool
    {
        return $this->isActual;
    }

    public function getBrandingType(): ?int
    {
        return $this->brandingType;
    }

    public function getContacts(): ?array
    {
        return $this->contacts;
    }

    public function getOperatingTime(): ?array
    {
        return $this->operatingTime;
    }

    public function getParameters(): ?Parameters
    {
        return $this->parameters;
    }

    public function isInternal(): ?bool
    {
        return $this->isInternal;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function getPhotos(): ?array
    {
        return $this->photos;
    }
}