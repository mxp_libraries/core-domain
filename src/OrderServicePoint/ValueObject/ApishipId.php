<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderServicePoint\ValueObject;

class ApishipId
{
    private $value;

    public function __construct(?string $value = null)
    {
        $this->value = $value;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }
}