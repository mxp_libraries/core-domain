<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderServicePoint\ValueObject;


class Contacts
{
    private $isPrimary;
    private $name;
    private $position;
    private $email;
    private $phone;
    private $innerPhone;

    public function __construct(
        ?bool $isPrimary = null,
        ?string $name = null,
        ?string $position = null,
        ?string $email = null,
        ?string $phone = null,
        ?string $innerPhone = null
    )
    {
        $this->isPrimary = $isPrimary;
        $this->name = $name;
        $this->position = $position;
        $this->email = $email;
        $this->phone = $phone;
        $this->innerPhone = $innerPhone;
    }

    public function isPrimary(): ?bool
    {
        return $this->isPrimary;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getInnerPhone(): ?string
    {
        return $this->innerPhone;
    }
}