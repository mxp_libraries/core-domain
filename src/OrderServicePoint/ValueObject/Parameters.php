<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderServicePoint\ValueObject;


class Parameters
{
    private $maxWeight;
    private $maxLength;
    private $maxHeight;
    private $maxWidth;
    private $storageDays;
    private $storageArea;
    private $deliveryZoneArea;

    public function __construct(
        ?int $maxWeight = null,
        ?int $maxLength = null,
        ?int $maxHeight = null,
        ?int $maxWidth = null,
        ?int $storageDays = null,
        ?int $storageArea = null,
        ?int $deliveryZoneArea = null

    )
    {
        $this->maxWeight = $maxWeight;
        $this->maxLength = $maxLength;
        $this->maxHeight = $maxHeight;
        $this->maxWidth = $maxWidth;
        $this->storageDays = $storageDays;
        $this->storageArea = $storageArea;
        $this->deliveryZoneArea = $deliveryZoneArea;
    }

    public function getMaxWeight(): ?int
    {
        return $this->maxWeight;
    }

    public function getMaxLength(): ?int
    {
        return $this->maxLength;
    }

    public function getMaxHeight(): ?int
    {
        return $this->maxHeight;
    }

    public function getMaxWidth(): ?int
    {
        return $this->maxWidth;
    }

    public function getStorageDays(): ?int
    {
        return $this->storageDays;
    }

    public function getStorageArea(): ?int
    {
        return $this->storageArea;
    }

    public function getDeliveryZoneArea(): ?int
    {
        return $this->deliveryZoneArea;
    }
}