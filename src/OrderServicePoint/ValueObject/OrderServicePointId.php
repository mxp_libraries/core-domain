<?php

namespace Maxipost\CoreDomain\OrderServicePoint\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class OrderServicePointId extends AggregateRootId
{
}
