<?php

namespace Maxipost\CoreDomain\DeliveryService;

use Maxipost\CoreDomain\DeliveryService\ValueObject\Category;
use Maxipost\CoreDomain\DeliveryService\ValueObject\DeliveryServiceCode;
use Maxipost\CoreDomain\DeliveryService\ValueObject\Unit;

class DeliveryService
{
    public const ASSESSMENT_TYPE_PERCENT = 0;
    public const ASSESSMENT_TYPE_MONEY = 1;
    public const ASSESSMENT_TYPES = [
        self::ASSESSMENT_TYPE_PERCENT => '%',
        self::ASSESSMENT_TYPE_MONEY => 'руб',
    ];
    public const SCOPE_ORDER = 'order';
    public const SCOPE_PICKUP = 'pickup';
    public const SCOPES = [
        self::SCOPE_ORDER,
        self::SCOPE_PICKUP,
    ];
    public const DELIVERY_SERVICES = [
        '501' => [
            'deliveryServiceCode' => '501',
            'name' => 'Страхование груза',
            'categoryId' => Category::ID_INSURANCE_PAYMENT,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_PERCENT],
            'isRequired' => true,
            'unitCodes' => [
                Unit::TYPE_COST,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'ОС товара',
        ],
        '502' => [
            'deliveryServiceCode' => '502',
            'name' => 'Оплата наличными',
            'categoryId' => Category::ID_INSURANCE_PAYMENT,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_PERCENT],
            'isRequired' => true,
            'unitCodes' => [
                Unit::TYPE_COST,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'НП заказа',
        ],
        '503' => [
            'deliveryServiceCode' => '503',
            'name' => 'Оплата картой',
            'categoryId' => Category::ID_INSURANCE_PAYMENT,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_PERCENT],
            'isRequired' => true,
            'unitCodes' => [
                Unit::TYPE_COST,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'НП заказа',
        ],
        '504' => [
            'deliveryServiceCode' => '504',
            'name' => 'Инкассация',
            'categoryId' => Category::ID_INSURANCE_PAYMENT,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_PERCENT],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_COST,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'НП заказа',
        ],

        '305' => [
            'deliveryServiceCode' => '305',
            'name' => 'Доставка до двери',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => true,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '306' => [
            'deliveryServiceCode' => '306',
            'name' => 'Выдача со склада',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '307' => [
            'deliveryServiceCode' => '307',
            'name' => 'Возврат заказа',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => true,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '308' => [
            'deliveryServiceCode' => '308',
            'name' => 'Забор',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => true,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [self::SCOPE_PICKUP],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '309' => [
            'deliveryServiceCode' => '309',
            'name' => 'Магистральная доставка',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '310' => [
            'deliveryServiceCode' => '310',
            'name' => 'Частичная выдача',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '311' => [
            'deliveryServiceCode' => '311',
            'name' => 'Вскрытие заводской упаковки',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '312' => [
            'deliveryServiceCode' => '312',
            'name' => 'Вскрытие транспортной упаковки',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '313' => [
            'deliveryServiceCode' => '313',
            'name' => 'Примерка',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '314' => [
            'deliveryServiceCode' => '314',
            'name' => 'Возврат документов',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '315' => [
            'deliveryServiceCode' => '315',
            'name' => 'Ожидание на адресе',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '316' => [
            'deliveryServiceCode' => '316',
            'name' => 'Доставка ко времени',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '317' => [
            'deliveryServiceCode' => '317',
            'name' => 'Доставка в день недели',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '318' => [
            'deliveryServiceCode' => '318',
            'name' => 'Повторная доставка',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '319' => [
            'deliveryServiceCode' => '319',
            'name' => 'Неуспешная попытка забора по вине курьера',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '320' => [
            'deliveryServiceCode' => '320',
            'name' => 'Неуспешная попытка забора по вине отправителя',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_PICKUP],
            'comment' => '',
        ],
        '321' => [
            'deliveryServiceCode' => '321',
            'name' => 'Неуспешная попытка доставки до двери по вине курьера',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '322' => [
            'deliveryServiceCode' => '322',
            'name' => 'Неуспешная попытка доставки до двери по вине получателя',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '323' => [
            'deliveryServiceCode' => '323',
            'name' => 'Неуспешная попытка доставки до склада по вине курьера',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '324' => [
            'deliveryServiceCode' => '324',
            'name' => 'Неуспешная попытка доставки до склада по вине получателя',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => '',
        ],
        '325' => [
            'deliveryServiceCode' => '325',
            'name' => 'Топливная надбавка',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '326' => [
            'deliveryServiceCode' => '326',
            'name' => 'Подъем на этаж с лифтом',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [self::SCOPE_ORDER],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '327' => [
            'deliveryServiceCode' => '327',
            'name' => 'Подъем на этаж без лифта',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '328' => [
            'deliveryServiceCode' => '328',
            'name' => 'Погрузо-разгрузочные работы',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
                self::SCOPE_PICKUP,
            ],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '329' => [
            'deliveryServiceCode' => '329',
            'name' => 'Пронос по территории',
            'categoryId' => Category::ID_DELIVERY,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_DISTANCE,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
                self::SCOPE_PICKUP,
            ],
            'comment' => '',
        ],

        '430' => [
            'deliveryServiceCode' => '430',
            'name' => 'Прозвон клиента',
            'categoryId' => Category::ID_INFORM,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '431' => [
            'deliveryServiceCode' => '431',
            'name' => 'SMS',
            'categoryId' => Category::ID_INFORM,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '432' => [
            'deliveryServiceCode' => '432',
            'name' => 'Сообщение в messenger',
            'categoryId' => Category::ID_INFORM,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '433' => [
            'deliveryServiceCode' => '433',
            'name' => 'Ручное редактировние заказа по просьбе клиента',
            'categoryId' => Category::ID_INFORM,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],

        '134' => [
            'deliveryServiceCode' => '134',
            'name' => 'Упаковка в пакет',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_DIMENSION,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => 'Справочник размера коробок и пакетов',
        ],
        '135' => [
            'deliveryServiceCode' => '135',
            'name' => 'Упаковка в коробку',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_DIMENSION,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => 'Справочник размера коробок и пакетов',
        ],
        '136' => [
            'deliveryServiceCode' => '136',
            'name' => 'Стрейч пленка',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '137' => [
            'deliveryServiceCode' => '137',
            'name' => 'Пупырчатая пленка',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '138' => [
            'deliveryServiceCode' => '138',
            'name' => 'Обрешетка',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '139' => [
            'deliveryServiceCode' => '139',
            'name' => 'Хранение',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '140' => [
            'deliveryServiceCode' => '140',
            'name' => 'Предпочтовая подготовка',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '141' => [
            'deliveryServiceCode' => '141',
            'name' => 'Вложение в заказ',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '142' => [
            'deliveryServiceCode' => '142',
            'name' => 'Составная сборка',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '143' => [
            'deliveryServiceCode' => '143',
            'name' => 'Сортировка/транзи',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '144' => [
            'deliveryServiceCode' => '144',
            'name' => 'Проверка работоспособности',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '145' => [
            'deliveryServiceCode' => '145',
            'name' => 'Проверка комплектности',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '146' => [
            'deliveryServiceCode' => '146',
            'name' => 'Раскомплектование',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '147' => [
            'deliveryServiceCode' => '147',
            'name' => 'Этикирование',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_AMOUNT,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => '',
        ],
        '148' => [
            'deliveryServiceCode' => '148',
            'name' => 'Прием на склад',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '149' => [
            'deliveryServiceCode' => '149',
            'name' => 'Отгрузка со склада',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '150' => [
            'deliveryServiceCode' => '150',
            'name' => 'Обмер веса',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
        '151' => [
            'deliveryServiceCode' => '151',
            'name' => 'Обмер габаритов',
            'categoryId' => Category::ID_WAREHOUSE,
            'assessmentType' => self::ASSESSMENT_TYPES[self::ASSESSMENT_TYPE_MONEY],
            'isRequired' => false,
            'unitCodes' => [
                Unit::TYPE_WEIGHT,
                Unit::TYPE_DIMENSION,
                Unit::TYPE_VOLUME,
            ],
            'scopes' => [
                self::SCOPE_ORDER,
            ],
            'comment' => 'Справочник интервалов весов, коэффициент объемного веса из договора, справочник ограничений дименсий',
        ],
    ];
    private $deliveryServiceCode;

    public function __construct(
        DeliveryServiceCode $deliveryServiceCode
    ) {
        $this->deliveryServiceCode = $deliveryServiceCode;
    }

    public function getDeliveryServiceCode(): DeliveryServiceCode
    {
        return $this->deliveryServiceCode;
    }

    public function getName(): string
    {
        return self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['name'];
    }

    public function getCategory(): Category
    {
        return new Category(self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['categoryId']);
    }

    public function getCategoryId(): int
    {
        return self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['categoryId'];
    }

    public function getAssessmentType(): string
    {
        return self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['assessmentType'];
    }

    public function isRequired(): bool
    {
        return self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['isRequired'];
    }

    public function getUnitCodes(): array
    {
        return self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['unitCodes'];
    }

    public function getUnits(): array
    {
        $result = self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['unitCodes'];
        foreach ($result as &$item) {
            $item = new Unit($item);
        }
        return $result;
    }

    public function getScopes(): array
    {
        return self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['scopes'];
    }

    public function getComment(): string
    {
        return self::DELIVERY_SERVICES[$this->deliveryServiceCode->getValue()]['comment'];
    }
}

