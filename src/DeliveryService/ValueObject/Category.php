<?php

namespace Maxipost\CoreDomain\DeliveryService\ValueObject;

class Category
{
    public const ID_WAREHOUSE = 1;
    public const ID_HIGHWAY = 2;
    public const ID_DELIVERY = 3;
    public const ID_INFORM = 4;
    public const ID_INSURANCE_PAYMENT = 5;
    public const TYPES = [
        self::ID_WAREHOUSE => [
            'id' => self::ID_WAREHOUSE,
            'name' => 'склад',
            'comment' => '',
        ],
        self::ID_HIGHWAY => [
            'id' => self::ID_HIGHWAY,
            'name' => 'магистраль',
            'comment' => '',
        ],
        self::ID_DELIVERY => [
            'id' => self::ID_DELIVERY,
            'name' => 'доставка',
            'comment' => '',
        ],
        self::ID_INFORM => [
            'id' => self::ID_INFORM,
            'name' => 'информирование',
            'comment' => '',
        ],
        self::ID_INSURANCE_PAYMENT => [
            'id' => self::ID_INSURANCE_PAYMENT,
            'name' => 'страхование/оплата',
            'comment' => '',
        ],
    ];
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id]['name'];
    }

    public function getComment(): string
    {
        return self::TYPES[$this->id]['comment'];
    }
}