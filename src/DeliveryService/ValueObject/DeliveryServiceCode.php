<?php

namespace Maxipost\CoreDomain\DeliveryService\ValueObject;

class DeliveryServiceCode
{
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
