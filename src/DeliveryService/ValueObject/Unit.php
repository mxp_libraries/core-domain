<?php

namespace Maxipost\CoreDomain\DeliveryService\ValueObject;

class Unit
{
    public const TYPE_WEIGHT = 1;
    public const TYPE_DIMENSION = 2;
    public const TYPE_VOLUME = 3;
    public const TYPE_TIME = 4;
    public const TYPE_COST = 5;
    public const TYPE_AMOUNT = 6;
    public const TYPE_DISTANCE = 7;
    public const TYPES = [
        self::TYPE_WEIGHT => [
            'id' => self::TYPE_WEIGHT,
            'name' => 'Вес',
            'unit' => 'гр.',
            'measurementObject' => 'Дименсии заказа',
        ],
        self::TYPE_DIMENSION => [
            'id' => self::TYPE_DIMENSION,
            'name' => 'Габарит',
            'unit' => 'см.',
            'measurementObject' => 'Дименсии заказа',
        ],
        self::TYPE_VOLUME => [
            'id' => self::TYPE_VOLUME,
            'name' => 'Объем',
            'unit' => 'см.куб.',
            'measurementObject' => 'Дименсии заказа',
        ],
        self::TYPE_TIME => [
            'id' => self::TYPE_TIME,
            'name' => 'Время',
            'unit' => 'мин.',
            'measurementObject' => 'Дата и время',
        ],
        self::TYPE_COST => [
            'id' => self::TYPE_COST,
            'name' => 'Стоимость',
            'unit' => 'руб.',
            'measurementObject' => 'COD заказа',
        ],
        self::TYPE_AMOUNT => [
            'id' => self::TYPE_AMOUNT,
            'name' => 'Количество',
            'unit' => 'шт.',
            'measurementObject' => 'Натуральное значение',
        ],
        self::TYPE_DISTANCE => [
            'id' => self::TYPE_DISTANCE,
            'name' => 'Расстояния',
            'unit' => 'м.',
            'measurementObject' => 'Пройденный путь',
        ],
    ];
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id]['name'];
    }

    public function getUnit(): string
    {
        return self::TYPES[$this->id]['unit'];
    }

    public function getMeasurementObject(): string
    {
        return self::TYPES[$this->id]['measurementObject'];
    }
}