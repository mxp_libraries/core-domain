<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\LegalPerson\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class LegalPersonId extends AggregateRootId
{
}
