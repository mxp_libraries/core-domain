<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\LegalPerson\ValueObject;

class BankDetails
{
    private $name;
    private $okpo;
    private $settlementAccount;
    private $corrAccount;
    private $bik;
    private $accountNumber;

    public function getName(): string
    {
        return $this->name;
    }

    public function getOkpo(): string
    {
        return $this->okpo;
    }

    public function getSettlementAccount(): string
    {
        return $this->settlementAccount;
    }

    public function getCorrAccount(): string
    {
        return $this->corrAccount;
    }

    public function getBik(): string
    {
        return $this->bik;
    }

    public function getAccountNumber(): string
    {
        return $this->accountNumber;
    }

    public function __construct
    (
        string $name,
        string $okpo,
        string $settlementAccount,
        string $corrAccount,
        string $bik,
        string $accountNumber
    ) {
        $this->name = $name;
        $this->okpo = $okpo;
        $this->settlementAccount = $settlementAccount;
        $this->corrAccount = $corrAccount;
        $this->bik = $bik;
        $this->accountNumber = $accountNumber;
    }
}