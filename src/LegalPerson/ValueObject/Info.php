<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\LegalPerson\ValueObject;

class Info
{
    private $realAddress;
    private $legalAddress;
    private $dadataInfo;

    public function getRealAddress(): string
    {
        return $this->realAddress;
    }

    public function getLegalAddress(): string
    {
        return $this->legalAddress;
    }

    public function getDadataInfo(): array
    {
        return $this->dadataInfo;
    }

    public function __construct
    (
        string $realAddress,
        string $legalAddress,
        array $dadataInfo
    ) {
        $this->realAddress = $realAddress;
        $this->legalAddress = $legalAddress;
        $this->dadataInfo = $dadataInfo;
    }
}