<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\LegalPerson\ValueObject;

use Assert\Assertion;

class Sno
{
    public const TYPES = [
        0 => 'УСН',
        1 => 'ОСН',
    ];
    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}