<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\LegalPerson\Event;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\LegalPerson\ValueObject\Info;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\DomainEventSourcing\DomainEvent;

class LegalPersonWasCreated extends DomainEvent
{
    private $isPrimary;
    private $inn;
    private $ogrn;
    private $kpp;
    private $name;
    private $type;
    private $sno;
    private $info;
    private $bankDetails;
    private $counterpartyId;

    public static function getEventId(): string
    {
        return 'legalPerson.wasCreated';
    }

    public function __construct(
        LegalPersonId $id,
        CounterpartyId $counterpartyId,
        string $name,
        ?bool $isPrimary = null,
        ?string $inn = null,
        ?string $ogrn = null,
        ?string $kpp = null,
        ?int $type = null,
        ?int $sno = null,
        ?Info $info = null,
        ?array $bankDetails = null
    ) {
        parent::__construct($id);

        $this->counterpartyId = $counterpartyId;
        $this->isPrimary = $isPrimary;
        $this->inn = $inn;
        $this->ogrn = $ogrn;
        $this->kpp = $kpp;
        $this->name = $name;
        $this->type = $type;
        $this->sno = $sno;
        $this->info = $info;
        $this->bankDetails = $bankDetails;
    }

    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isPrimary(): ?bool
    {
        return $this->isPrimary;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function getOgrn(): ?string
    {
        return $this->ogrn;
    }

    public function getKpp(): ?string
    {
        return $this->kpp;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function getSno(): ?int
    {
        return $this->sno;
    }

    public function getInfo(): ?Info
    {
        return $this->info;
    }

    public function getBankDetails(): ?array
    {
        return $this->bankDetails;
    }
}