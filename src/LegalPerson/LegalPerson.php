<?php

namespace Maxipost\CoreDomain\LegalPerson;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\LegalPerson\ValueObject\BankDetails;
use Maxipost\CoreDomain\LegalPerson\ValueObject\Info;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;

class LegalPerson
{
    private $isPrimary;
    private $inn;
    private $ogrn;
    private $kpp;
    private $name;
    private $type;
    private $sno;
    private $info;
    private $bankDetails;
    private $counterpartyId;
    private $id;

    /**
     * LegalPerson constructor.
     * @param CounterpartyId $counterpartyId
     * @param string $name
     * @param bool|null $isPrimary
     * @param string|null $inn
     * @param string|null $ogrn
     * @param string|null $kpp
     * @param int|null $type
     * @param int|null $sno
     * @param Info|null $info
     * @param BankDetails[]|null $bankDetails
     */
    public function __construct(
        CounterpartyId $counterpartyId,
        string $name,
        ?bool $isPrimary = null,
        ?string $inn = null,
        ?string $ogrn = null,
        ?string $kpp = null,
        ?int $type = null,
        ?int $sno = null,
        ?Info $info = null,
        ?array $bankDetails = []
    ) {

        $this->counterpartyId = $counterpartyId;
        $this->name = $name;
        $this->isPrimary = $isPrimary;
        $this->inn = $inn;
        $this->ogrn = $ogrn;
        $this->kpp = $kpp;
        $this->type = $type;
        $this->sno = $sno;
        $this->info = $info;
        $this->bankDetails = $bankDetails;
    }

    /**
     * @return LegalPersonId
     */
    public function getId(): LegalPersonId
    {
        return $this->id;
    }

    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function isPrimary(): ?bool
    {
        return $this->isPrimary;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function getOgrn(): ?string
    {
        return $this->ogrn;
    }

    public function getKpp(): ?string
    {
        return $this->kpp;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function getSno(): ?int
    {
        return $this->sno;
    }

    public function getInfo(): ?Info
    {
        return $this->info;
    }

    /**
     * @return BankDetails[]
     */
    public function getBankDetails(): array
    {
        return $this->bankDetails;
    }
}
