<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\DeliveryZone;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\DeliveryZone\ValueObject\DeliveryZoneId;

class DeliveryZone
{
    private $counterpartyId;
    private $name;
    private $deliveryZoneCode;
    private $comment;
    private $isActual;
    private $geoJsonData;
    private $id;

    public function __construct
    (
        CounterpartyId $counterpartyId,
        string $name,
        string $deliveryZoneCode,
        bool $isActual,
        array $geoJsonData,
        ?string $comment = null
    ) {
        $this->counterpartyId = $counterpartyId;
        $this->name = $name;
        $this->deliveryZoneCode = $deliveryZoneCode;
        $this->comment = $comment;
        $this->isActual = $isActual;
        $this->geoJsonData = $geoJsonData;
    }

    public function getId(): DeliveryZoneId
    {
        return $this->id;
    }

    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDeliveryZoneCode(): string
    {
        return $this->deliveryZoneCode;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function isActual(): bool
    {
        return $this->isActual;
    }

    public function getGeoJsonData(): array
    {
        return $this->geoJsonData;
    }
}
