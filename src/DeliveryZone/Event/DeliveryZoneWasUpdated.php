<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\DeliveryZone\Event;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\DeliveryZone\ValueObject\DeliveryZoneId;
use Maxipost\DomainEventSourcing\DomainEvent;


class DeliveryZoneWasUpdated extends DomainEvent
{

    private $counterpartyId;
    private $name;
    private $deliveryZoneCode;
    private $comment;
    private $isActual;
    private $geoJsonData;

    public static function getEventId(): string
    {
        return 'deliveryZone.wasCreated';
    }

    public function __construct(
        DeliveryZoneId $id,
        CounterpartyId $counterpartyId,
        string $name,
        string $deliveryZoneCode,
        bool $isActual,
        array $geoJsonData,
        ?string $comment = null
    )
    {
        parent::__construct($id);

        $this->counterpartyId = $counterpartyId;
        $this->name = $name;
        $this->deliveryZoneCode = $deliveryZoneCode;
        $this->comment = $comment;
        $this->isActual = $isActual;
        $this->geoJsonData = $geoJsonData;
    }

    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDeliveryZoneCode(): string
    {
        return $this->deliveryZoneCode;
    }

    public function isActual(): bool
    {
        return $this->isActual;
    }

    public function getGeoJsonData(): array
    {
        return $this->geoJsonData;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }


}
