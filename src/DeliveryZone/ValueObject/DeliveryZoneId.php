<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\DeliveryZone\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class DeliveryZoneId extends AggregateRootId
{

}