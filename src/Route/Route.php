<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Route;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Route\ValueObject\CurrencyId;
use Maxipost\CoreDomain\Route\ValueObject\DangerSubtypeId;
use Maxipost\CoreDomain\Route\ValueObject\DangerTypeId;
use Maxipost\CoreDomain\Route\ValueObject\RouteId;
use Maxipost\CoreDomain\Route\ValueObject\Schedule;
use Maxipost\CoreDomain\Route\ValueObject\TransportTypeId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;

class Route
{
    private $counterpartyId;
    private $isActual;
    private $fromWarehouseId;
    private $toWarehouseId;
    private $transportTypeId;
    /**
     * @var DangerTypeId[]|null
     */
    private $dangerTypeId;
    /**
     * @var DangerSubtypeId[]|null
     */
    private $dangerSubtypeId;
    private $currencyId;
    private $averageDeliveryTime;
    private $minWeight;
    private $maxWeight;
    private $volume;
    private $price;
    private $schedule;
    /**
     * @var RouteId
     */
    private $id;

    /**
     * Route constructor.
     * @param CounterpartyId $counterpartyId
     * @param bool $isActual
     * @param WarehouseId $fromWarehouseId
     * @param WarehouseId $toWarehouseId
     * @param TransportTypeId $transportTypeId
     * @param int $price
     * @param float $minWeight
     * @param float $maxWeight
     * @param float $volume
     * @param DangerTypeId[]|null $dangerTypeId
     * @param DangerSubtypeId[]|null $dangerSubtypeId
     * @param CurrencyId|null $currencyId
     * @param int|null $averageDeliveryTime
     * @param Schedule[]|null $schedule
     */
    public function __construct
    (
        CounterpartyId $counterpartyId,
        bool $isActual,
        WarehouseId $fromWarehouseId,
        WarehouseId $toWarehouseId,
        TransportTypeId $transportTypeId,
        int $price,
        float $minWeight,
        float $maxWeight,
        float $volume,
        ?array $dangerTypeId = null,
        ?array $dangerSubtypeId = null,
        ?CurrencyId $currencyId = null,
        ?int $averageDeliveryTime = null,
        ?array $schedule = null
    ) {
        $this->counterpartyId = $counterpartyId;
        $this->isActual = $isActual;
        $this->fromWarehouseId = $fromWarehouseId;
        $this->toWarehouseId = $toWarehouseId;
        $this->transportTypeId = $transportTypeId;
        $this->price = $price;
        $this->minWeight = $minWeight;
        $this->maxWeight = $maxWeight;
        $this->volume = $volume;
        $this->dangerTypeId = $dangerTypeId;
        $this->dangerSubtypeId = $dangerSubtypeId;
        $this->currencyId = $currencyId;
        $this->averageDeliveryTime = $averageDeliveryTime;
        $this->schedule = $schedule;
    }

    /**
     * @return CounterpartyId
     */
    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function isActual(): bool
    {
        return $this->isActual;
    }

    public function getFromWarehouseId(): WarehouseId
    {
        return $this->fromWarehouseId;
    }

    public function getToWarehouseId(): WarehouseId
    {
        return $this->toWarehouseId;
    }

    public function getTransportTypeId(): TransportTypeId
    {
        return $this->transportTypeId;
    }

    /**
     * @return DangerTypeId[]|null
     */
    public function getDangerTypeId(): ?array
    {
        return $this->dangerTypeId;
    }

    /**
     * @return DangerSubtypeId[]|null
     */
    public function getDangerSubtypeId(): ?array
    {
        return $this->dangerSubtypeId;
    }

    public function getCurrencyId(): ?CurrencyId
    {
        return $this->currencyId;
    }

    public function getAverageDeliveryTime(): ?int
    {
        return $this->averageDeliveryTime;
    }

    public function getMinWeight(): float
    {
        return $this->minWeight;
    }

    public function getMaxWeight(): float
    {
        return $this->maxWeight;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return Schedule[]|null
     */
    public function getSchedule(): ?array
    {
        return $this->schedule;
    }

    public function getId(): RouteId
    {
        return $this->id;
    }
}
