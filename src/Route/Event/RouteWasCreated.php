<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Route\Event;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Route\ValueObject\CurrencyId;
use Maxipost\CoreDomain\Route\ValueObject\RouteId;
use Maxipost\CoreDomain\Route\ValueObject\TransportTypeId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\DomainEventSourcing\DomainEvent;

class RouteWasCreated extends DomainEvent
{
    private $counterpartyId;
    private $isActual;
    private $fromWarehouseId;
    private $toWarehouseId;
    private $transportTypeId;
    private $dangerTypeId;
    private $dangerSubtypeId;
    private $currencyId;
    private $averageDeliveryTime;
    private $minWeight;
    private $schedule;
    private $price;
    private $maxWeight;
    private $volume;

    public static function getEventId(): string
    {
        return 'route.wasCreated';
    }

    public function __construct(
        RouteId $id,
        CounterpartyId $counterpartyId,
        bool $isActual,
        WarehouseId $fromWarehouseId,
        WarehouseId $toWarehouseId,
        TransportTypeId $transportTypeId,
        int $price,
        float $minWeight,
        float $maxWeight,
        float $volume,
        ?array $dangerTypeId = null,
        ?array $dangerSubtypeId = null,
        ?CurrencyId $currencyId = null,
        ?int $averageDeliveryTime = null,
        ?array $schedule = null
    ) {
        parent::__construct($id);
        $this->counterpartyId = $counterpartyId;
        $this->isActual = $isActual;
        $this->fromWarehouseId = $fromWarehouseId;
        $this->toWarehouseId = $toWarehouseId;
        $this->transportTypeId = $transportTypeId;
        $this->dangerTypeId = $dangerTypeId;
        $this->dangerSubtypeId = $dangerSubtypeId;
        $this->currencyId = $currencyId;
        $this->averageDeliveryTime = $averageDeliveryTime;
        $this->minWeight = $minWeight;
        $this->maxWeight = $maxWeight;
        $this->schedule = $schedule;
        $this->toWarehouseId = $toWarehouseId;
        $this->price = $price;
        $this->volume = $volume;
    }

    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function isActual(): bool
    {
        return $this->isActual;
    }

    public function getFromWarehouseId(): WarehouseId
    {
        return $this->fromWarehouseId;
    }

    public function getToWarehouseId(): WarehouseId
    {
        return $this->toWarehouseId;
    }

    public function getTransportTypeId(): TransportTypeId
    {
        return $this->transportTypeId;
    }

    public function getMinWeight(): float
    {
        return $this->minWeight;
    }

    public function getMaxWeight(): float
    {
        return $this->maxWeight;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }

    public function getDangerTypeId(): ?array
    {
        return $this->dangerTypeId;
    }

    public function getDangerSubtypeId(): ?array
    {
        return $this->dangerSubtypeId;
    }

    public function getCurrencyId(): ?CurrencyId
    {
        return $this->currencyId;
    }

    public function getAverageDeliveryTime(): ?int
    {
        return $this->averageDeliveryTime;
    }

    public function getSchedule(): ?array
    {
        return $this->schedule;
    }
}
