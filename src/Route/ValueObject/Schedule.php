<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Route\ValueObject;

class Schedule
{
    private $from;
    private $to;

    public function __construct
    (
        array $from,
        array $to
    ) {
        $this->from = $from;
        $this->to = $to;
    }

    public function getFrom(): array
    {
        return $this->from;
    }

    public function getTo(): array
    {
        return $this->to;
    }
}