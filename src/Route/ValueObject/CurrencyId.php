<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Route\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class CurrencyId extends AggregateRootId
{
}