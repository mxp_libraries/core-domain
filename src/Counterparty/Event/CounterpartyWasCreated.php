<?php

namespace Maxipost\CoreDomain\Counterparty\Event;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Counterparty\ValueObject\Integration;
use Maxipost\DomainEventSourcing\DomainEvent;

class CounterpartyWasCreated extends DomainEvent
{
    private $isClient;
    private $isContractor;
    private $name;
    private $logoUrl;
    private $contacts;
    /**
     * @var Integration[]
     */
    private $integration;

    public static function getEventId(): string
    {
        return 'counterparty.wasCreated';
    }

    public function __construct(
        CounterpartyId $id,
        bool $isClient,
        bool $isContractor,
        string $name,
        array $integration,
        ?string $logoUrl = null,
        ?array $contacts = null
    )
    {
        parent::__construct($id);

        $this->isClient = $isClient;
        $this->isContractor = $isContractor;
        $this->name = $name;
        $this->integration = $integration;
        $this->logoUrl = $logoUrl;
        $this->contacts = $contacts;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isClient(): bool
    {
        return $this->isClient;
    }

    public function isContractor(): bool
    {
        return $this->isContractor;
    }

    /**
     * @return array Integration[]
     */
    public function getIntegration(): array
    {
        return $this->integration;
    }

    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    public function getContacts(): ?array
    {
        return $this->contacts;
    }
}
