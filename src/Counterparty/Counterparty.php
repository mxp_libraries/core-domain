<?php

namespace Maxipost\CoreDomain\Counterparty;

use Maxipost\CoreDomain\Counterparty\ValueObject\Contact;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;

class Counterparty
{
    private $isClient;
    private $isContractor;
    private $name;
    private $logoUrl;
    /**
     * @var Contact[]|null
     */
    private $contacts;
    private $id;

    /**
     * CreateCounterpartyDto constructor.
     * @param bool $isClient
     * @param bool $isContractor
     * @param string $name
     * @param string $logoUrl
     * @param Contact[]|null $contacts
     */
    public function __construct
    (
        bool $isClient,
        bool $isContractor,
        string $name,
        ?string $logoUrl,
        ?array $contacts
    )
    {
        $this->isClient = $isClient;
        $this->isContractor = $isContractor;
        $this->name = $name;
        $this->logoUrl = $logoUrl;
        $this->contacts = $contacts;
    }

    public function getId(): CounterpartyId
    {
        return $this->id;
    }

    public function isClient(): bool
    {
        return $this->isClient;
    }

    public function isContractor(): bool
    {
        return $this->isContractor;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    /**
     * @return Contact[]
     */
    public function getContacts(): ?array
    {
        return $this->contacts;
    }
}