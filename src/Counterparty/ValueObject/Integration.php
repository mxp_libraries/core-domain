<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Counterparty\ValueObject;

class Integration
{

    private $providerKey;
    private $orderServicePointTariffId;
    private $courierTariffId;
    private $providerConnectIds;

    public function __construct
    (
        string $providerKey,
        string $orderServicePointTariffId,
        string $courierTariffId,
        array $providerConnectIds
    )
    {
        $this->providerKey = $providerKey;
        $this->orderServicePointTariffId = $orderServicePointTariffId;
        $this->courierTariffId = $courierTariffId;
        $this->providerConnectIds = $providerConnectIds;
    }

    public function getProviderKey(): string
    {
        return $this->providerKey;
    }

    public function getOrderServicePointTariffId(): string
    {
        return $this->orderServicePointTariffId;
    }

    public function getCourierTariffId(): string
    {
        return $this->courierTariffId;
    }

    public function getProviderConnectIds(): array
    {
        return $this->providerConnectIds;
    }

}