<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Counterparty\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\Contact\Role;

class Contact
{
    private $email;
    private $position;
    private $phoneNumber;
    private $fio;
    private $role;

    public function __construct(
        string $position,
        string $phoneNumber,
        string $fio,
        Role $role,
        ?string $email = null
    )
    {
        $this->email = $email;
        $this->position = $position;
        $this->phoneNumber = $phoneNumber;
        $this->fio = $fio;
        $this->role = $role;
    }

    public function getPosition(): string
    {
        return $this->position;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function getFio(): string
    {
        return $this->fio;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }
}
