<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Warehouse\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class WarehouseId extends AggregateRootId
{
}
