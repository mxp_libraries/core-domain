<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Warehouse\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\Contact\Role;

class Contact
{
    private $email;
    private $position;
    private $phone;
    private $name;
    private $role;

    public function __construct(
        string $phone,
        string $name,
        ?string $email,
        ?string $position,
        ?Role $role
    ) {
        $this->email = $email;
        $this->position = $position;
        $this->phone = $phone;
        $this->name = $name;
        $this->role = $role;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPosition(): string
    {
        return $this->position;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRole(): Role
    {
        return $this->role;
    }
}
