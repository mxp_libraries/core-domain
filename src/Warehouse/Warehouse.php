<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Warehouse;

use Maxipost\CoreDomain\Common\ValueObject\Address;
use Maxipost\CoreDomain\Common\ValueObject\InternalCode;
use Maxipost\CoreDomain\Common\ValueObject\OperatingTime;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Warehouse\ValueObject\ApishipId;
use Maxipost\CoreDomain\Warehouse\ValueObject\Contact;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;

class Warehouse
{
    private $name;
    private $address;
    private $operatingTime;
    private $warehouseApishipId;
    private $counterpartyId;
    private $internalCode;
    private $providerCode;
    private $isActual;
    private $contacts;
    private $id;

    /**
     * Warehouse constructor.
     * @param ApishipId|null $warehouseApishipId
     * @param CounterpartyId|null $counterpartyId
     * @param string|null $providerCode
     * @param bool|null $isActual
     * @param string|null $name
     * @param Address|null $address
     * @param ValueObject\Contact[]|null $contacts
     * @param OperatingTime[]|null $operatingTime
     */
    public function __construct(
        ?ApishipId $warehouseApishipId = null,
        ?CounterpartyId $counterpartyId = null,
        ?string $providerCode = null,
        ?bool $isActual = null,
        ?string $name = null,
        ?Address $address = null,
        ?array $contacts = null,
        ?array $operatingTime = null
    ) {
        $this->warehouseApishipId = $warehouseApishipId;
        $this->counterpartyId = $counterpartyId;
        $this->providerCode = $providerCode;
        $this->isActual = $isActual;
        $this->name = $name;
        $this->address = $address;
        $this->contacts = $contacts;
        $this->operatingTime = $operatingTime;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    /**
     * @return OperatingTime[]|null
     */
    public function getOperatingTime(): ?array
    {
        return $this->operatingTime;
    }

    public function getWarehouseApishipId(): ?ApishipId
    {
        return $this->warehouseApishipId;
    }

    public function getCounterpartyId(): ?CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function getInternalCode(): ?InternalCode
    {
        return $this->internalCode;
    }

    public function getProviderCode(): ?string
    {
        return $this->providerCode;
    }

    public function isActual(): ?bool
    {
        return $this->isActual;
    }

    /**
     * @return Contact[]|null
     */
    public function getContacts(): ?array
    {
        return $this->contacts;
    }

    public function getId(): WarehouseId
    {
        return $this->id;
    }
}