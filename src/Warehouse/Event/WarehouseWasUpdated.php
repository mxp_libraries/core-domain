<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Warehouse\Event;

use Maxipost\CoreDomain\Common\ValueObject\Address;
use Maxipost\CoreDomain\Common\ValueObject\InternalCode;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Warehouse\ValueObject\ApishipId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\DomainEventSourcing\DomainEvent;

class WarehouseWasUpdated extends DomainEvent
{
    private $name;
    private $address;
    private $operatingTime;
    private $warehouseApishipId;
    private $counterpartyId;
    private $internalCode;
    private $providerCode;
    private $isActual;
    private $contacts;

    public static function getEventId(): string
    {
        return 'warehouse.wasUpdated';
    }

    public function __construct(
        WarehouseId $id,
        ?ApishipId $warehouseApishipId = null,
        ?CounterpartyId $counterpartyId = null,
        ?InternalCode $internalCode = null,
        ?string $providerCode = null,
        ?bool $isActual = null,
        ?string $name = null,
        ?Address $address = null,
        ?array $contacts = null,
        ?array $operatingTime = null
    ) {
        parent::__construct($id);
        $this->name = $name;
        $this->address = $address;
        $this->operatingTime = $operatingTime;
        $this->warehouseApishipId = $warehouseApishipId;
        $this->counterpartyId = $counterpartyId;
        $this->internalCode = $internalCode;
        $this->providerCode = $providerCode;
        $this->isActual = $isActual;
        $this->contacts = $contacts;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function getOperatingTime(): ?array
    {
        return $this->operatingTime;
    }

    public function getWarehouseApishipId(): ?ApishipId
    {
        return $this->warehouseApishipId;
    }

    public function getCounterpartyId(): ?CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function getInternalCode(): ?InternalCode
    {
        return $this->internalCode;
    }

    public function getProviderCode(): ?string
    {
        return $this->providerCode;
    }

    public function isActual(): ?bool
    {
        return $this->isActual;
    }

    public function getContacts(): ?array
    {
        return $this->contacts;
    }
}
