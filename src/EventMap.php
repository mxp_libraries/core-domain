<?php


namespace Maxipost\CoreDomain;


use Maxipost\CoreDomain\Contract\Event\ContractWasCreated;
use Maxipost\CoreDomain\Contract\Event\ContractWasUpdated;
use Maxipost\CoreDomain\Counterparty\Event\CounterpartyWasCreated;
use Maxipost\CoreDomain\Counterparty\Event\CounterpartyWasUpdated;
use Maxipost\CoreDomain\Courier\Event\CourierWasCreated;
use Maxipost\CoreDomain\Courier\Event\CourierWasUpdated;
use Maxipost\CoreDomain\DeliveryZone\Event\DeliveryZoneWasCreated;
use Maxipost\CoreDomain\DeliveryZone\Event\DeliveryZoneWasUpdated;
use Maxipost\CoreDomain\LegalPerson\Event\LegalPersonWasCreated;
use Maxipost\CoreDomain\LegalPerson\Event\LegalPersonWasUpdated;
use Maxipost\CoreDomain\Order\Event\OrderWasCreated;
use Maxipost\CoreDomain\Order\Event\OrderWasUpdated;
use Maxipost\CoreDomain\OrderProviderStatus\Event\OrderProviderStatusWasCreated;
use Maxipost\CoreDomain\OrderServicePoint\Event\OrderServicePointWasCreated;
use Maxipost\CoreDomain\OrderServicePoint\Event\OrderServicePointWasUpdated;
use Maxipost\CoreDomain\Route\Event\RouteWasCreated;
use Maxipost\CoreDomain\Route\Event\RouteWasUpdated;
use Maxipost\CoreDomain\Warehouse\Event\WarehouseWasCreated;
use Maxipost\CoreDomain\Warehouse\Event\WarehouseWasUpdated;

class EventMap
{

    private $map;

    public static function getDefaultMap(): array
    {
        return [
            ContractWasCreated::getEventId() => ContractWasCreated::class,
            ContractWasUpdated::getEventId() => ContractWasUpdated::class,
            CounterpartyWasCreated::getEventId() => CounterpartyWasCreated::class,
            CounterpartyWasUpdated::getEventId() => CounterpartyWasUpdated::class,
            CourierWasCreated::getEventId() => CourierWasCreated::class,
            CourierWasUpdated::getEventId() => CourierWasUpdated::class,
            DeliveryZoneWasCreated::getEventId() => DeliveryZoneWasCreated::class,
            DeliveryZoneWasUpdated::getEventId() => DeliveryZoneWasUpdated::class,
            LegalPersonWasCreated::getEventId() => LegalPersonWasCreated::class,
            LegalPersonWasUpdated::getEventId() => LegalPersonWasUpdated::class,
            OrderWasCreated::getEventId() => OrderWasCreated::class,
            OrderWasUpdated::getEventId() => OrderWasUpdated::class,
            OrderProviderStatusWasCreated::getEventId() => OrderProviderStatusWasCreated::class,
            OrderServicePointWasCreated::getEventId() => OrderServicePointWasCreated::class,
            OrderServicePointWasUpdated::getEventId() => OrderServicePointWasUpdated::class,
            RouteWasCreated::getEventId() => RouteWasCreated::class,
            RouteWasUpdated::getEventId() => RouteWasUpdated::class,
            WarehouseWasCreated::getEventId() => WarehouseWasCreated::class,
            WarehouseWasUpdated::getEventId() => WarehouseWasUpdated::class,
        ];
    }

    public function __construct(array $map = null)
    {
        if ($map === null) {
            $map = self::getDefaultMap();
        }
        $this->map = $map;
    }

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }
}