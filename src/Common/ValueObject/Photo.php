<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Common\ValueObject;

/**
 * Class Photo
 * @package Maxipost\CoreDomain\Common\ValueObject
 */
class Photo
{
    /**
     * @var string
     */
    private $value;

    /**
     * Photo constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}