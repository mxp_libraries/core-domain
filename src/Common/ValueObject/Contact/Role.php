<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Common\ValueObject\Contact;

class Role
{
    public const TYPES = [
        0 => 'Менеджер',
        1 => 'Руководитель',
    ];
    /**
     * @var int
     */
    private $id;

    /**
     * Role constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}