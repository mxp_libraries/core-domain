<?php

namespace Maxipost\CoreDomain\Common\ValueObject;

class TimeInterval
{
    private $from;
    private $to;

    public function __construct(Time $from, Time $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function getFrom(): Time
    {
        return $this->from;
    }

    public function getTo(): Time
    {
        return $this->to;
    }
}
