<?php

namespace Maxipost\CoreDomain\Common\ValueObject;

class DateTimeIntervalSimple
{
    private $date;
    private $timeInterval;

    public function __construct(
        ?Date $date = null,
        ?TimeInterval $timeInterval = null
    ) {
        $this->date = $date;
        $this->timeInterval = $timeInterval;
    }

    public function getDate(): ?Date
    {
        return $this->date;
    }

    public function getTimeInterval(): ?TimeInterval
    {
        return $this->timeInterval;
    }
}
