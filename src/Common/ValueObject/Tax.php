<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Common\ValueObject;

class Tax
{
    public const TYPES = [
        1 => [
            'id' => 1,
            'countryCode' => 'RU',
            'name' => 'VAT',
            'value' => 'без НДС',
            'isActual' => true,
        ],
        2 => [
            'id' => 2,
            'countryCode' => 'RU',
            'name' => 'VAT',
            'value' => '0',
            'isActual' => true,
        ],
        3 => [
            'id' => 3,
            'countryCode' => 'RU',
            'name' => 'VAT',
            'value' => '10%',
            'isActual' => true,
        ],
        4 => [
            'id' => 4,
            'countryCode' => 'RU',
            'name' => 'VAT',
            'value' => '18%',
            'isActual' => false,
        ],
        5 => [
            'id' => 5,
            'countryCode' => 'RU',
            'name' => 'VAT',
            'value' => '20%',
            'isActual' => true,
        ],
    ];
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id]['name'];
    }

    public function getValue(): string
    {
        return self::TYPES[$this->id]['value'];
    }

    public function isActual(): bool
    {
        return self::TYPES[$this->id]['isActual'];
    }
}