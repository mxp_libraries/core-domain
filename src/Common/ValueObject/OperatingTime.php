<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Common\ValueObject;

class OperatingTime
{
    private $dayOfWeekIndex;
    private $openedFrom;
    private $openedTo;
    private $breakFrom;
    private $breakTo;

    /**
     * OperatingTime constructor.
     * @param int|null $dayOfWeekIndex
     * @param Time|null $openedFrom
     * @param Time|null $openedTo
     * @param Time|null $breakFrom
     * @param Time|null $breakTo
     */
    public function __construct(
        ?int $dayOfWeekIndex = null,
        ?Time $openedFrom = null,
        ?Time $openedTo = null,
        ?Time $breakFrom = null,
        ?Time $breakTo = null
    ) {
        $this->dayOfWeekIndex = $dayOfWeekIndex;
        $this->openedFrom = $openedFrom;
        $this->openedTo = $openedTo;
        $this->breakFrom = $breakFrom;
        $this->breakTo = $breakTo;
    }

    public function getDayOfWeekIndex(): ?int
    {
        return $this->dayOfWeekIndex;
    }

    public function getOpenedFrom(): ?Time
    {
        return $this->openedFrom;
    }

    public function getOpenedTo(): ?Time
    {
        return $this->openedTo;
    }

    public function getBreakFrom(): ?Time
    {
        return $this->breakFrom;
    }

    public function getBreakTo(): ?Time
    {
        return $this->breakTo;
    }
}
