<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Common\ValueObject;

class InternalCode
{
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}