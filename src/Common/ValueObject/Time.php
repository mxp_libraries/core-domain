<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Common\ValueObject;

/**
 * Class Time
 * @package Maxipost\CoreDomain\Common\ValueObject
 */
class Time
{
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
