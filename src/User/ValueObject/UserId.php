<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\User\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;
use Maxipost\DomainEventSourcing\Exception\InvalidUUIDException;

class UserId extends AggregateRootId
{

    /**
     * @param mixed $uuid
     */
    protected function setUuid($uuid): void
    {
        if ($uuid === null) {
            throw new InvalidUUIDException();
        }
        $this->uuid = $uuid;
    }
}