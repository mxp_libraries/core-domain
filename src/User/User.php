<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\User;

use Maxipost\CoreDomain\User\ValueObject\Phone;
use Maxipost\CoreDomain\User\ValueObject\UserId;

class User
{
    protected $_id;
    /**
     * @var \Maxipost\CoreDomain\User\ValueObject\Permissions
     */
    protected $permissions;
    /**
     * @var \Maxipost\CoreDomain\User\ValueObject\Role[]
     */
    protected $roles = [];
    /**
     * @var string
     */
    protected $password;
    /**
     * @var \Maxipost\CoreDomain\User\ValueObject\Phone
     */
    protected $phone;

    /**
     * User constructor.
     * @param \Maxipost\CoreDomain\User\ValueObject\UserId $id
     * @param \Maxipost\CoreDomain\User\ValueObject\Role[] $roles
     * @param \Maxipost\CoreDomain\User\ValueObject\Permissions $permissions
     * @param \Maxipost\CoreDomain\User\ValueObject\Phone $phone
     * @param string|null $password
     */
    public function __construct(
        UserId $id,
        array $roles,
        ValueObject\Permissions $permissions,
        ValueObject\Phone $phone,
        ?string $password
    ) {
        $this->permissions = $permissions;
        $this->roles = $roles;
        $this->phone = $phone;
        $this->password = $password;
        $this->_id = $id;
    }

    public function getId(): ValueObject\UserId
    {
        return $this->_id;
    }

    /**
     * @return \Maxipost\CoreDomain\User\ValueObject\Permissions
     */
    public function getPermissions(): ValueObject\Permissions
    {
        return $this->permissions;
    }

    /**
     * @return \Maxipost\CoreDomain\User\ValueObject\Role[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function hasRole(string $roleName): bool
    {
        foreach ($this->roles as $role) {
            if ($role->getValue() === $roleName) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return \Maxipost\CoreDomain\User\ValueObject\Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }
}