<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\User\Event;

use Maxipost\CoreDomain\User\ValueObject\Permissions;
use Maxipost\CoreDomain\User\ValueObject\Phone;
use Maxipost\DomainEventSourcing\AggregateRootId;
use Maxipost\DomainEventSourcing\DomainEvent;

class UserWasCreated extends DomainEvent
{
    /**
     * @var \Maxipost\CoreDomain\User\ValueObject\Permissions
     */
    private $permissions;
    /**
     * @var array|\Maxipost\CoreDomain\User\ValueObject\Role[]
     */
    private $roles;
    /**
     * @var \Maxipost\CoreDomain\User\ValueObject\Phone
     */
    private $phone;

    /**
     * UserWasCreated constructor.
     * @param \Maxipost\DomainEventSourcing\AggregateRootId $userId
     * @param \Maxipost\CoreDomain\User\ValueObject\Role[] $roles
     * @param \Maxipost\CoreDomain\User\ValueObject\Permissions $permissions
     * @param \Maxipost\CoreDomain\User\ValueObject\Phone $phone
     */
    public function __construct(
        AggregateRootId $userId,
        array $roles,
        Permissions $permissions,
        Phone $phone
    ) {
        parent::__construct($userId);
        $this->permissions = $permissions;
        $this->roles = $roles;
        $this->phone = $phone;
    }

    public static function getEventId(): string
    {
        return 'user.wasCreated';
    }

    /**
     * @return \Maxipost\CoreDomain\User\ValueObject\Permissions
     */
    public function getPermissions(): Permissions
    {
        return $this->permissions;
    }

    /**
     * @return array|\Maxipost\CoreDomain\User\ValueObject\Role[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return \Maxipost\CoreDomain\User\ValueObject\Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }
}