<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderProviderStatus\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class OrderProviderStatusId extends AggregateRootId
{
}