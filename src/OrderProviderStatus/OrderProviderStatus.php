<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderProviderStatus;

use DateTimeImmutable;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;

class OrderProviderStatus
{
    private $orderId;
    private $counterpartyId;
    private $providerCreatedAt;
    private $providerStatusCode;

    private $providerStatusName;

    private $providerSubstatusCode;
    private $providerStatusDescription;
    private $createdAt;

    public function __construct(
        OrderId $orderId,
        CounterpartyId $counterpartyId,
        DateTimeImmutable $providerCreatedAt,
        string $providerStatusCode,
        string $providerStatusName,
        string $providerSubstatusCode,
        string $providerStatusDescription
    ) {
        $this->orderId = $orderId;
        $this->counterpartyId = $counterpartyId;
        $this->providerCreatedAt = $providerCreatedAt;
        $this->providerStatusCode = $providerStatusCode;
        $this->providerStatusName = $providerStatusName;
        $this->providerSubstatusCode = $providerSubstatusCode;
        $this->providerStatusDescription = $providerStatusDescription;
    }

    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }

    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function getProviderCreatedAt(): DateTimeImmutable
    {
        return $this->providerCreatedAt;
    }

    public function getProviderStatusCode(): string
    {
        return $this->providerStatusCode;
    }

    public function getProviderStatusName(): string
    {
        return $this->providerStatusName;
    }

    public function getProviderSubstatusCode(): string
    {
        return $this->providerSubstatusCode;
    }

    public function getProviderStatusDescription(): string
    {
        return $this->providerStatusDescription;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}