<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderProviderStatus\Event;

use DateTimeImmutable;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\CoreDomain\OrderProviderStatus\ValueObject;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderProviderStatusWasCreated extends DomainEvent
{
    private $orderId;
    private $counterpartyId;
    private $providerCreatedAt;
    private $providerStatusCode;
    private $providerStatusName;
    private $providerSubstatusCode;
    private $providerStatusDescription;
    private $createdAt;

    public static function getEventId(): string
    {
        return 'OrderProviderStatus.wasCreated';
    }

    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }

    public function getCounterpartyId(): CounterpartyId
    {
        return $this->counterpartyId;
    }

    public function getProviderCreatedAt(): DateTimeImmutable
    {
        return $this->providerCreatedAt;
    }

    public function getProviderStatusCode(): string
    {
        return $this->providerStatusCode;
    }

    public function getProviderStatusName(): string
    {
        return $this->providerStatusName;
    }

    public function getProviderSubstatusCode(): string
    {
        return $this->providerSubstatusCode;
    }

    public function getProviderStatusDescription(): string
    {
        return $this->providerStatusDescription;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function __construct
    (
        ValueObject\OrderProviderStatusId $id,
        OrderId $orderId ,
        CounterpartyId $counterpartyId ,
        DateTimeImmutable $providerCreatedAt,
        string $providerStatusCode,
        string $providerStatusName,
        string $providerSubstatusCode,
        string $providerStatusDescription,
        DateTimeImmutable $createdAt
    )
    {
        parent::__construct($id);
        $this->orderId = $orderId;
        $this->counterpartyId = $counterpartyId;
        $this->providerCreatedAt = $providerCreatedAt;
        $this->providerStatusCode = $providerStatusCode;
        $this->providerStatusName = $providerStatusName;
        $this->providerSubstatusCode = $providerSubstatusCode;
        $this->providerStatusDescription = $providerStatusDescription;
        $this->createdAt = $createdAt;
    }
}