<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderGroup;

use DateTimeImmutable;
use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\CoreDomain\OrderGroup\ValueObject;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;

class OrderGroup
{
    private $id;
    private $type;
    private $orderIds;
    private $courierId;
    private $internalNumber;
    private $status;
    private $fromWarehouseId;
    private $toWarehouseId;
    private $createdAt;
    private $sentAt;
    private $receivedAt;

    /**
     * @param ValueObject\Type $type
     * @param OrderId[] $orderIds
     * @param CourierId $courierId
     * @param string $internalNumber
     * @param ValueObject\Status $status
     * @param WarehouseId $fromWarehouseId
     * @param WarehouseId $toWarehouseId
     * @param \DateTimeImmutable $createdAt
     * @param \DateTimeImmutable $sentAt
     * @param \DateTimeImmutable $receivedAt
     */
    public function __construct(
        ValueObject\Type $type,
        array $orderIds,
        ?CourierId $courierId = null,
        ?string $internalNumber = null,
        ?ValueObject\Status $status = null,
        ?WarehouseId $fromWarehouseId = null,
        ?WarehouseId $toWarehouseId = null,
        ?DateTimeImmutable $createdAt = null,
        ?DateTimeImmutable $sentAt = null,
        ?DateTimeImmutable $receivedAt = null
    )
    {
        $this->type = $type;
        $this->orderIds = $orderIds;
        $this->courierId = $courierId;
        $this->internalNumber = $internalNumber;
        $this->status = $status;
        $this->fromWarehouseId = $fromWarehouseId;
        $this->toWarehouseId = $toWarehouseId;
        $this->createdAt = $createdAt;
        $this->sentAt = $sentAt;
        $this->receivedAt = $receivedAt;
    }

    public function getId(): ValueObject\OrderGroupId
    {
        return $this->id;
    }

    public function getType(): ValueObject\Type
    {
        return $this->type;
    }

    /**
     * @return array|OrderId[]
     */
    public function getOrderIds(): array
    {
        return $this->orderIds;
    }

    public function getCourierId(): CourierId
    {
        return $this->courierId;
    }

    public function getInternalNumber(): ?string
    {
        return $this->internalNumber;
    }

    public function getStatus(): ?ValueObject\Status
    {
        return $this->status;
    }

    public function getFromWarehouseId(): ?WarehouseId
    {
        return $this->fromWarehouseId;
    }

    public function getToWarehouseId(): ?WarehouseId
    {
        return $this->toWarehouseId;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getSentAt(): ?DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function getReceivedAt(): ?DateTimeImmutable
    {
        return $this->receivedAt;
    }
}