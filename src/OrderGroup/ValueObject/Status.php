<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderGroup\ValueObject;

use Assert\Assertion;

class Status
{
    public const ORDER_GROUP_CREATED = 0;
    public const ORDER_GROUP_SENT = 1;
    public const ORDER_GROUP_RECEIVED = 2;
    public const TYPES = [
        self::ORDER_GROUP_CREATED => 'Партия создана',
        self::ORDER_GROUP_SENT => 'Партия отправлена',
        self::ORDER_GROUP_RECEIVED => 'Партия получена',
    ];
    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}