<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderGroup\ValueObject;

use Assert\Assertion;

class Type
{
    public const TYPES = [
        0 => 'Заказы к доставке',
        1 => 'Заказы к возврату',
    ];
    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}