<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderGroup\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class OrderGroupId extends AggregateRootId
{
}