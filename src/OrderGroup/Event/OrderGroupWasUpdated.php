<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderGroup\Event;

use DateTimeImmutable;
use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\CoreDomain\OrderGroup\ValueObject;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderGroupWasUpdated extends DomainEvent
{
    private $type;
    private $orderIds;
    private $courierId;
    private $internalNumber;
    private $status;
    private $fromWarehouseId;
    private $toWarehouseId;
    private $createdAt;
    private $sentAt;
    private $receivedAt;

    /**
     * String event id. For example: order.wasCreated
     * @return string
     */
    public static function getEventId(): string
    {
        return 'orderGroup.wasUpdated';
    }

    /**
     * OrderGroupWasCreated constructor.
     * @param ValueObject\OrderGroupId $id
     * @param ValueObject\Type $type
     * @param OrderId[] $orderIds
     * @param CourierId|null $courierId
     * @param string|null $internalNumber
     * @param ValueObject\Status|null $status
     * @param WarehouseId|null $fromWarehouseId
     * @param WarehouseId|null $toWarehouseId
     * @param \DateTimeImmutable|null $createdAt
     * @param \DateTimeImmutable|null $sentAt
     * @param \DateTimeImmutable|null $receivedAt
     */
    public function __construct(
        ValueObject\OrderGroupId $id,
        ValueObject\Type $type,
        array $orderIds,
        ?CourierId $courierId = null,
        ?string $internalNumber = null,
        ?ValueObject\Status $status = null,
        ?WarehouseId $fromWarehouseId = null,
        ?WarehouseId $toWarehouseId = null,
        ?DateTimeImmutable $createdAt = null,
        ?DateTimeImmutable $sentAt = null,
        ?DateTimeImmutable $receivedAt = null
    ) {
        parent::__construct($id);

        $this->type = $type;
        $this->orderIds = $orderIds;
        $this->courierId = $courierId;
        $this->internalNumber = $internalNumber;
        $this->status = $status;
        $this->fromWarehouseId = $fromWarehouseId;
        $this->toWarehouseId = $toWarehouseId;
        $this->createdAt = $createdAt;
        $this->sentAt = $sentAt;
        $this->receivedAt = $receivedAt;
    }

    public function getType(): ValueObject\Type
    {
        return $this->type;
    }

    /**
     * @return array|OrderId[]
     */
    public function getOrderIds(): array
    {
        return $this->orderIds;
    }

    public function getCourierId(): ?CourierId
    {
        return $this->courierId;
    }

    public function getInternalNumber(): ?string
    {
        return $this->internalNumber;
    }

    public function getStatus(): ?ValueObject\Status
    {
        return $this->status;
    }

    public function getFromWarehouseId(): ?WarehouseId
    {
        return $this->fromWarehouseId;
    }

    public function getToWarehouseId(): ?WarehouseId
    {
        return $this->toWarehouseId;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getSentAt(): ?DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function getReceivedAt(): ?DateTimeImmutable
    {
        return $this->receivedAt;
    }
}