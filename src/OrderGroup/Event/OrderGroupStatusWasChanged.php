<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderGroup\Event;

use Maxipost\CoreDomain\OrderGroup\ValueObject;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderGroupStatusWasChanged extends DomainEvent
{
    private $status;

    public static function getEventId(): string
    {
        return 'orderGroup.status.wasUpdated';
    }

    public function __construct(
        ValueObject\OrderGroupId $id,
        ValueObject\Status $status
    ) {
        parent::__construct($id);
        $this->status = $status;
    }

    public function getStatus(): ValueObject\Status
    {
        return $this->status;
    }
}