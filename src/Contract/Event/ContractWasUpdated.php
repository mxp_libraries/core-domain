<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\Event;

use Maxipost\CoreDomain\Common\ValueObject\Tax;
use Maxipost\CoreDomain\Contract\ValueObject\ContractId;
use Maxipost\CoreDomain\Contract\ValueObject\ContractType;
use Maxipost\CoreDomain\Contract\ValueObject\CostReductionFactors;
use Maxipost\CoreDomain\Contract\ValueObject\CounterpartyType;
use Maxipost\CoreDomain\Contract\ValueObject\Delivery;
use Maxipost\CoreDomain\Contract\ValueObject\MoneyTransferType;
use Maxipost\CoreDomain\Contract\ValueObject\NettingTariffType;
use Maxipost\CoreDomain\Contract\ValueObject\NettingType;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\DomainEventSourcing\DomainEvent;

class ContractWasUpdated extends DomainEvent
{
    private $clientLegalPersonId;
    private $contractorLegalPersonId;
    private $countryCode;
    private $taxId;
    private $counterpartyType;
    private $isActual;
    private $date;
    private $number;
    private $providerNumber;
    private $nettingType;
    private $nettingPercent;
    private $nettingTariffType;
    private $nettingTariffPercent;
    private $moneyTransferType;
    private $delivery;
    private $costReductionFactors;
    private $contractServices;
    private $volume;
    private $dangerTypes;
    /**
     * @var ContractType
     */
    private $contractType;

    public static function getEventId(): string
    {
        return 'contract.wasCreated';
    }

    public function __construct(
        ContractId $id,
        LegalPersonId $clientLegalPerson,
        LegalPersonId $contractorLegalPerson,
        string $countryCode,
        Tax $taxId,
        ContractType $contractType,
        bool $isActual,
        ?CounterpartyType $counterpartyType = null,
        ?string $date = null,
        ?string $number = null,
        ?string $providerNumber = null,
        ?NettingType $nettingType = null,
        ?int $nettingPercent = null,
        ?NettingTariffType $nettingTariffType = null,
        ?int $nettingTariffPercent = null,
        ?MoneyTransferType $moneyTransferType = null,
        ?Delivery $delivery = null,
        ?CostReductionFactors $costReductionFactors = null,
        ?float $volume = null,
        ?array $contractServices = null,
        ?array $dangerTypes = null
    ) {
        parent::__construct($id);

        $this->clientLegalPersonId = $clientLegalPerson;
        $this->contractorLegalPersonId = $contractorLegalPerson;
        $this->counterpartyType = $counterpartyType;
        $this->isActual = $isActual;
        $this->date = $date;
        $this->contractType = $contractType;
        $this->number = $number;
        $this->providerNumber = $providerNumber;
        $this->nettingType = $nettingType;
        $this->nettingPercent = $nettingPercent;
        $this->nettingTariffType = $nettingTariffType;
        $this->nettingTariffPercent = $nettingTariffPercent;
        $this->moneyTransferType = $moneyTransferType;
        $this->delivery = $delivery;
        $this->costReductionFactors = $costReductionFactors;
        $this->contractServices = $contractServices;
        $this->countryCode = $countryCode;
        $this->taxId = $taxId;
        $this->volume = $volume;
        $this->dangerTypes = $dangerTypes;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function getTaxId(): Tax
    {
        return $this->taxId;
    }

    public function getClientLegalPersonId(): LegalPersonId
    {
        return $this->clientLegalPersonId;
    }

    public function getContractorLegalPersonId(): LegalPersonId
    {
        return $this->contractorLegalPersonId;
    }

    public function getContractType(): ContractType
    {
        return $this->contractType;
    }

    public function isActual(): bool
    {
        return $this->isActual;
    }

    public function getCounterpartyType(): ?CounterpartyType
    {
        return $this->counterpartyType;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function getProviderNumber(): ?string
    {
        return $this->providerNumber;
    }

    public function getNettingType(): ?NettingType
    {
        return $this->nettingType;
    }

    public function getNettingPercent(): ?int
    {
        return $this->nettingPercent;
    }

    public function getNettingTariffType(): ?NettingTariffType
    {
        return $this->nettingTariffType;
    }

    public function getNettingTariffPercent(): ?int
    {
        return $this->nettingTariffPercent;
    }

    public function getMoneyTransferType(): ?MoneyTransferType
    {
        return $this->moneyTransferType;
    }

    public function getDelivery(): ?Delivery
    {
        return $this->delivery;
    }

    public function getCostReductionFactors(): ?CostReductionFactors
    {
        return $this->costReductionFactors;
    }

    public function getContractServices(): ?array
    {
        return $this->contractServices;
    }

    public function getVolume(): ?float
    {
        return $this->volume;
    }

    public function getDangerTypes(): ?array
    {
        return $this->dangerTypes;
    }
}
