<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\ValueObject;

class ContractService
{
    private $serviceName;
    private $serviceCategoryId;
    private $cost;

    public function __construct(
        string $serviceName,
        ServiceCategory $serviceCategoryId,
        int $cost
    ) {
        $this->serviceName = $serviceName;
        $this->serviceCategoryId = $serviceCategoryId;
        $this->cost = $cost;
    }

    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    public function getServiceCategoryId(): ServiceCategory
    {
        return $this->serviceCategoryId;
    }

    public function getCost(): int
    {
        return $this->cost;
    }
}
