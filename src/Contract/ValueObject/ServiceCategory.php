<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\ValueObject;

use function array_keys;
use function array_map;

class ServiceCategory
{
    public const TYPES = [
        0 => [
            'id' => 0,
            'name' => 'default',
        ],
        1 => [
            'id' => 1,
            'name' => 'склад',
        ],
        2 => [
            'id' => 2,
            'name' => 'магистраль',
        ],
        3 => [
            'id' => 3,
            'name' => 'доставка',
        ],
        4 => [
            'id' => 4,
            'name' => 'информирование',
        ],
        5 => [
            'id' => 5,
            'name' => 'страхование/оплата',
        ],
    ];
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id]['name'];
    }

    public static function getTypeIds(): array
    {
        return array_map(static function ($id) {
            return (string)$id;
        }, array_keys(self::TYPES));
    }
}