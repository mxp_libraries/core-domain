<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\ValueObject;

class CostReductionFactors
{
    private $multiplier1Min;
    private $multiplier1Max;
    private $multiplier1;
    private $multiplier2Min;
    private $multiplier2Max;
    private $multiplier2;
    private $multiplier3Min;
    private $multiplier3Max;
    private $multiplier3;

    public function __construct(
        int $multiplier1Min,
        int $multiplier1Max,
        int $multiplier1,
        int $multiplier2Min,
        int $multiplier2Max,
        int $multiplier2,
        int $multiplier3Min,
        int $multiplier3Max,
        int $multiplier3
    ) {
        $this->multiplier1Min = $multiplier1Min;
        $this->multiplier1Max = $multiplier1Max;
        $this->multiplier1 = $multiplier1;
        $this->multiplier2Min = $multiplier2Min;
        $this->multiplier2Max = $multiplier2Max;
        $this->multiplier2 = $multiplier2;
        $this->multiplier3Min = $multiplier3Min;
        $this->multiplier3Max = $multiplier3Max;
        $this->multiplier3 = $multiplier3;
    }

    public function getMultiplier1Min(): int
    {
        return $this->multiplier1Min;
    }

    public function getMultiplier1Max(): int
    {
        return $this->multiplier1Max;
    }

    public function getMultiplier1(): int
    {
        return $this->multiplier1;
    }

    public function getMultiplier2Min(): int
    {
        return $this->multiplier2Min;
    }

    public function getMultiplier2Max(): int
    {
        return $this->multiplier2Max;
    }

    public function getMultiplier2(): int
    {
        return $this->multiplier2;
    }

    public function getMultiplier3Min(): int
    {
        return $this->multiplier3Min;
    }

    public function getMultiplier3Max(): int
    {
        return $this->multiplier3Max;
    }

    public function getMultiplier3(): int
    {
        return $this->multiplier3;
    }
}
