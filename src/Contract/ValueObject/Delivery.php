<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\ValueObject;

class Delivery
{
    private $dangerTypes;
    private $storageDays;
    private $volume;

    public function __construct(
        int $dangerTypes = null,
        int $storageDays = null,
        int $volume = null
    ) {
        $this->dangerTypes = $dangerTypes;
        $this->storageDays = $storageDays;
        $this->volume = $volume;
    }

    public function getDangerTypes(): int
    {
        return $this->dangerTypes;
    }

    public function getStorageDays(): int
    {
        return $this->storageDays;
    }

    public function getVolume(): int
    {
        return $this->volume;
    }
}
