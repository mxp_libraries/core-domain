<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class ContractId extends AggregateRootId
{
}
