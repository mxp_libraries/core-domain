<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\ValueObject;

use Assert\Assertion;

class MoneyTransferType
{
    public const TYPES = [
        0 => 'Наличный',
        1 => 'Безналичный',
    ];
    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}