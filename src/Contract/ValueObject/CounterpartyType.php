<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\ValueObject;

use Assert\Assertion;

class CounterpartyType
{
    public const TYPES = [
        0 => 'Подрядчик',
        1 => 'Клиент',
    ];
    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}