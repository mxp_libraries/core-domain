<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract\ValueObject;

use Assert\Assertion;

class NettingType
{
    public const TYPES = [
        0 => 'Зачем 100%',
        1 => 'Счет',
        2 => 'Зачем НП',
    ];
    private $id;

    public function __construct(int $id)
    {
        Assertion::keyExists(self::TYPES, $id);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id];
    }
}