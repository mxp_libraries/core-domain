<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Contract;

use Maxipost\CoreDomain\Common\ValueObject\Tax;
use Maxipost\CoreDomain\Contract\ValueObject\ContractId;
use Maxipost\CoreDomain\Contract\ValueObject\ContractService;
use Maxipost\CoreDomain\Contract\ValueObject\ContractType;
use Maxipost\CoreDomain\Contract\ValueObject\CostReductionFactors;
use Maxipost\CoreDomain\Contract\ValueObject\CounterpartyType;
use Maxipost\CoreDomain\Contract\ValueObject\Delivery;
use Maxipost\CoreDomain\Contract\ValueObject\MoneyTransferType;
use Maxipost\CoreDomain\Contract\ValueObject\NettingTariffType;
use Maxipost\CoreDomain\Contract\ValueObject\NettingType;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\CoreDomain\Route\ValueObject\DangerTypeId;

class Contract
{
    private $clientLegalPersonId;
    private $contractorLegalPersonId;
    private $countryCode;
    private $taxId;
    private $contractType;
    private $date;
    private $number;
    private $providerNumber;
    private $nettingType;
    private $moneyTransferType;
    private $counterpartyType;
    private $isActual;
    private $nettingPercent;
    private $nettingTariffType;
    private $nettingTariffPercent;
    private $delivery;
    private $costReductionFactors;
    private $contractServices;
    private $dangerTypes;
    private $volume;
    private $id;

    /**
     * CreateContractDto constructor.
     * @param LegalPersonId $clientLegalPersonId
     * @param LegalPersonId $contractorLegalPersonId
     * @param string $countryCode
     * @param Tax $taxId
     * @param ContractType $contractType
     * @param bool $isActual
     * @param CounterpartyType|null $counterpartyType
     * @param string|null $date
     * @param string|null $number
     * @param string|null $providerNumber
     * @param NettingType|null $nettingType
     * @param int|null $nettingPercent
     * @param NettingTariffType|null $nettingTariffType
     * @param int|null $nettingTariffPercent
     * @param MoneyTransferType|null $moneyTransferType
     * @param Delivery|null $delivery
     * @param CostReductionFactors|null $costReductionFactors
     * @param float|null $volume
     * @param ContractService[]|null $contractServices
     * @param DangerTypeId[]|null $dangerTypes
     */
    public function __construct(
        LegalPersonId $clientLegalPersonId,
        LegalPersonId $contractorLegalPersonId,
        string $countryCode,
        Tax $taxId,
        ContractType $contractType,
        bool $isActual,
        ?CounterpartyType $counterpartyType = null,
        ?string $date = null,
        ?string $number = null,
        ?string $providerNumber = null,
        ?NettingType $nettingType = null,
        ?int $nettingPercent = null,
        ?NettingTariffType $nettingTariffType = null,
        ?int $nettingTariffPercent = null,
        ?MoneyTransferType $moneyTransferType = null,
        ?Delivery $delivery = null,
        ?CostReductionFactors $costReductionFactors = null,
        ?float $volume = null,
        ?array $contractServices = null,
        ?array $dangerTypes = null
    ) {
        $this->clientLegalPersonId = $clientLegalPersonId;
        $this->contractorLegalPersonId = $contractorLegalPersonId;
        $this->countryCode = $countryCode;
        $this->taxId = $taxId;
        $this->contractType = $contractType;
        $this->isActual = $isActual;
        $this->counterpartyType = $counterpartyType;
        $this->date = $date;
        $this->number = $number;
        $this->providerNumber = $providerNumber;
        $this->nettingType = $nettingType;
        $this->nettingPercent = $nettingPercent;
        $this->nettingTariffType = $nettingTariffType;
        $this->nettingTariffPercent = $nettingTariffPercent;
        $this->moneyTransferType = $moneyTransferType;
        $this->delivery = $delivery;
        $this->costReductionFactors = $costReductionFactors;
        $this->volume = $volume;
        $this->contractServices = $contractServices;
        $this->dangerTypes = $dangerTypes;
    }

    public function getId(): ContractId
    {
        return $this->id;
    }

    public function getClientLegalPersonId(): LegalPersonId
    {
        return $this->clientLegalPersonId;
    }

    public function getContractorLegalPersonId(): LegalPersonId
    {
        return $this->contractorLegalPersonId;
    }

    public function getContractType(): ContractType
    {
        return $this->contractType;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function getProviderNumber(): ?string
    {
        return $this->providerNumber;
    }

    public function getNettingType(): ?NettingType
    {
        return $this->nettingType;
    }

    public function getMoneyTransferType(): ?MoneyTransferType
    {
        return $this->moneyTransferType;
    }

    public function getCounterpartyType(): ?CounterpartyType
    {
        return $this->counterpartyType;
    }

    public function isActual(): bool
    {
        return $this->isActual;
    }

    public function getNettingPercent(): ?int
    {
        return $this->nettingPercent;
    }

    public function getNettingTariffType(): ?NettingTariffType
    {
        return $this->nettingTariffType;
    }

    public function getNettingTariffPercent(): ?int
    {
        return $this->nettingTariffPercent;
    }

    public function getDelivery(): ?Delivery
    {
        return $this->delivery;
    }

    public function getCostReductionFactors(): ?CostReductionFactors
    {
        return $this->costReductionFactors;
    }

    /**
     * @return ContractService[]
     */
    public function getContractServices(): ?array
    {
        return $this->contractServices;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function getTaxId(): Tax
    {
        return $this->taxId;
    }

    public function getDangerTypes(): ?array
    {
        return $this->dangerTypes;
    }

    public function getVolume(): ?float
    {
        return $this->volume;
    }
}