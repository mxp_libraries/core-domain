<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Courier;

use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\Courier\ValueObject\Credentials;
use Maxipost\CoreDomain\Courier\ValueObject\Document;
use Maxipost\CoreDomain\Courier\ValueObject\Transport;
use Maxipost\CoreDomain\User\ValueObject\UserId;
use Maxipost\CoreDomain\Courier\ValueObject\Permissions;

class Courier
{
    private $id;
    private $isActive;
    private $fullname;
    private $shortname;
    private $mainPhone;
    private $additionalPhone;
    private $livingArea;
    private $credentials;
    private $transport;
    /**
     * @var Document[]|null
     */
    private $documents;
    /**
     * @var \Maxipost\CoreDomain\User\ValueObject\UserId
     */
    private $userId;

    /**
     * @var \Maxipost\CoreDomain\Courier\ValueObject\Permissions
     */
    private $permissions;
    /**
     * CreateCourierDto constructor.
     * @param \Maxipost\CoreDomain\User\ValueObject\UserId $userId
     * @param bool $isActive
     * @param string $fullname
     * @param string $shortname
     * @param string $mainPhone
     * @param string $additionalPhone
     * @param string $livingArea
     * @param Credentials|null $credentials
     * @param Transport|null $transport
     * @param Document[]|null $documents
     * @param Permissions $permissions
     */
    public function __construct
    (
        UserId $userId,
        bool $isActive,
        string $fullname,
        string $shortname,
        string $mainPhone,
        ?string $additionalPhone = null,
        ?string $livingArea = null,
        ?Credentials $credentials = null,
        ?Transport $transport = null,
        ?array $documents = null,
        Permissions $permissions
    ) {
        $this->isActive = $isActive;
        $this->fullname = $fullname;
        $this->shortname = $shortname;
        $this->mainPhone = $mainPhone;
        $this->additionalPhone = $additionalPhone;
        $this->livingArea = $livingArea;
        $this->credentials = $credentials;
        $this->transport = $transport;
        $this->documents = $documents;
        $this->userId = $userId;
        $this->permissions = $permissions;
    }

    public function getId(): CourierId
    {
        return $this->id;
    }

    public function getUserId(): UserId
    {
        return $this->userId;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getFullname(): string
    {
        return $this->fullname;
    }

    public function getShortname(): string
    {
        return $this->shortname;
    }

    public function getMainPhone(): string
    {
        return $this->mainPhone;
    }

    public function getAdditionalPhone(): ?string
    {
        return $this->additionalPhone;
    }

    public function getLivingArea(): ?string
    {
        return $this->livingArea;
    }

    public function getCredentials(): ?Credentials
    {
        return $this->credentials;
    }

    public function getTransport(): ?Transport
    {
        return $this->transport;
    }

    /**
     * @return Document[]|null
     */
    public function getDocuments(): ?array
    {
        return $this->documents;
    }

    /**
     * @return \Maxipost\CoreDomain\Courier\ValueObject\Permissions
     */
    public function getPermissions(): Permissions
    {
        return $this->permissions;
    }
}
