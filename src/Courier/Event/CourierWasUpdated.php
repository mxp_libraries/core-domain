<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Courier\Event;

use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\Courier\ValueObject\Credentials;
use Maxipost\CoreDomain\Courier\ValueObject\Document;
use Maxipost\CoreDomain\Courier\ValueObject\Transport;
use Maxipost\DomainEventSourcing\DomainEvent;

class CourierWasUpdated extends DomainEvent
{
    private $isActive;
    private $fullname;
    private $shortname;
    private $mainPhone;
    private $additionalPhone;
    private $livingArea;
    private $credentials;
    private $transport;
    private $documents;

    public static function getEventId(): string
    {
        return 'courier.wasUpdated';
    }

    /**
     * CourierWasCreated constructor.
     * @param CourierId $id
     * @param bool $isActive
     * @param string $fullname
     * @param string $shortname
     * @param string $mainPhone
     * @param string|null $additionalPhone
     * @param string|null $livingArea
     * @param Credentials|null $credentials
     * @param Transport|null $transport
     * @param Document[] $documents
     */
    public function __construct(
        CourierId $id,
        bool $isActive,
        string $fullname,
        string $shortname,
        string $mainPhone,
        ?string $additionalPhone,
        ?string $livingArea,
        ?Credentials $credentials,
        ?Transport $transport,
        ?array $documents
    ) {
        parent::__construct($id);

        $this->isActive = $isActive;
        $this->fullname = $fullname;
        $this->shortname = $shortname;
        $this->mainPhone = $mainPhone;
        $this->additionalPhone = $additionalPhone;
        $this->livingArea = $livingArea;
        $this->credentials = $credentials;
        $this->transport = $transport;
        $this->documents = $documents;
    }

    public function getDocuments(): array
    {
        return $this->documents;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getFullname(): string
    {
        return $this->fullname;
    }

    public function getShortname(): string
    {
        return $this->shortname;
    }

    public function getMainPhone(): string
    {
        return $this->mainPhone;
    }

    public function getAdditionalPhone(): ?string
    {
        return $this->additionalPhone;
    }

    public function getLivingArea(): ?string
    {
        return $this->livingArea;
    }

    public function getCredentials(): ?Credentials
    {
        return $this->credentials;
    }

    public function getTransport(): ?Transport
    {
        return $this->transport;
    }
}
