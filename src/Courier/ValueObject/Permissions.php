<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Courier\ValueObject;

class Permissions
{
    /**
     * @var array|\Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId[]
     */
    private $legalPersonIds;
    /**
     * @var array|\Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId[]
     */
    private $warehouseIds;

    /**
     * Permissions constructor.
     * @param \Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId[] $legalPersonIds
     * @param \Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId[] $warehouseIds
     */
    public function __construct(array $legalPersonIds, array $warehouseIds)
    {
        $this->legalPersonIds = $legalPersonIds;
        $this->warehouseIds = $warehouseIds;
    }

    /**
     * @return array|\Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId[]
     */
    public function getLegalPersonIds(): array
    {
        return $this->legalPersonIds;
    }

    /**
     * @return array|\Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId[]
     */
    public function getWarehouseIds(): array
    {
        return $this->warehouseIds;
    }
}