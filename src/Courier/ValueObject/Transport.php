<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Courier\ValueObject;

class Transport
{
    private $mark;
    private $model;
    private $registrationNumber;

    public function __construct(
        ?string $mark,
        ?string $model,
        ?string $registrationNumber
    ) {
        $this->mark = $mark;
        $this->model = $model;
        $this->registrationNumber = $registrationNumber;
    }

    public function getMark(): ?string
    {
        return $this->mark;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function getRegistrationNumber(): ?string
    {
        return $this->registrationNumber;
    }
}