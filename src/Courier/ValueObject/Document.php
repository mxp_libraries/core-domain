<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Courier\ValueObject;

use DateTimeImmutable;

class Document
{
    private $name;
    private $timestamp;
    private $data;

    public function __construct(
        string $name,
        DateTimeImmutable $timestamp,
        string $data
    ) {
        $this->name = $name;
        $this->timestamp = $timestamp;
        $this->data = $data;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTimestamp(): DateTimeImmutable
    {
        return $this->timestamp;
    }

    public function getData(): string
    {
        return $this->data;
    }
}