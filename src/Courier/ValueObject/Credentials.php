<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\Courier\ValueObject;

use Maxipost\CoreDomain\Common\ValueObject\Date;

class Credentials
{
    private $documentSerial;
    private $documentAuthority;
    private $birthday;

    public function __construct(
        ?string $documentSerial,
        ?string $documentAuthority,
        ?Date $birthday
    ) {
        $this->documentSerial = $documentSerial;
        $this->documentAuthority = $documentAuthority;
        $this->birthday = $birthday;
    }

    public function getDocumentSerial(): ?string
    {
        return $this->documentSerial;
    }

    public function getDocumentAuthority(): ?string
    {
        return $this->documentAuthority;
    }

    public function getBirthday(): ?Date
    {
        return $this->birthday;
    }
}