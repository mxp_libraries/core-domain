<?php

declare(strict_types=1);

namespace Maxipost\CoreDomain\Courier\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;

class CourierId extends AggregateRootId
{
}
