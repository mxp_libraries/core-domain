<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderStatusModel\ValueObject;

class OrderStatusModelId
{
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}