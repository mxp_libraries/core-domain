<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderStatusConstructorModel\Event;

use Maxipost\CoreDomain\Contract\ValueObject\ServiceCategory;
use Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject\ExternalContractor;
use Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject\OrderStatusConstructorModelId;
use Maxipost\DomainEventSourcing\DomainEvent;

class OrderStatusConstructorModelWasUpdated extends DomainEvent
{
    private $serviceCategoryId;
    private $name;
    private $isVisibleForContractor;
    private $isVisibleForClient;
    private $isEnd;
    private $isEditable;
    private $isActual;
    private $externalContractors;

    public static function getEventId(): string
    {
        return 'orderStatusConstructorModel.wasUpdated';
    }

    /**
     * OrderStatusConstructorModelWasUpdated constructor.
     * @param OrderStatusConstructorModelId $id
     * @param ServiceCategory $serviceCategoryId
     * @param string $name
     * @param bool $isVisibleForContractor
     * @param bool $isVisibleForClient
     * @param bool $isEnd
     * @param bool $isEditable
     * @param bool $isActual
     * @param ExternalContractor[] $externalContractors
     */
    public function __construct(
        OrderStatusConstructorModelId $id,
        ServiceCategory $serviceCategoryId,
        string $name,
        bool $isVisibleForContractor,
        bool $isVisibleForClient,
        bool $isEnd,
        bool $isEditable,
        bool $isActual,
        array $externalContractors
    )
    {
        parent::__construct($id);
        $this->serviceCategoryId = $serviceCategoryId;
        $this->name = $name;
        $this->isVisibleForContractor = $isVisibleForContractor;
        $this->isVisibleForClient = $isVisibleForClient;
        $this->isEnd = $isEnd;
        $this->isEditable = $isEditable;
        $this->isActual = $isActual;
        $this->externalContractors = $externalContractors;
    }

    public function getServiceCategoryId(): ServiceCategory
    {
        return $this->serviceCategoryId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isVisibleForContractor(): bool
    {
        return $this->isVisibleForContractor;
    }

    public function isVisibleForClient(): bool
    {
        return $this->isVisibleForClient;
    }

    public function isEnd(): bool
    {
        return $this->isEnd;
    }

    public function isEditable(): bool
    {
        return $this->isEditable;
    }

    public function isActual(): bool
    {
        return $this->isActual;
    }

    /**
     * @return ExternalContractor[]
     */
    public function getExternalContractors(): array
    {
        return $this->externalContractors;
    }
}