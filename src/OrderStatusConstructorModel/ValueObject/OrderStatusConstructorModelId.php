<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject;

use Maxipost\DomainEventSourcing\AggregateRootId;
use Maxipost\DomainEventSourcing\Exception\InvalidUUIDException;

class OrderStatusConstructorModelId extends AggregateRootId
{

    public static function buildFromParams(Stage $stageId, Status $statusId, SubStatus $subStatusId): self
    {
        return new self(sprintf("%d%'.02d%'.03d", $stageId->getId(), $statusId->getId(), $subStatusId->getId()));
    }

    /**
     * @param mixed $uuid
     */
    protected function setUuid($uuid): void
    {
        if ($uuid === null) {
            throw new InvalidUUIDException();
        }
        $this->uuid = $uuid;
    }
}