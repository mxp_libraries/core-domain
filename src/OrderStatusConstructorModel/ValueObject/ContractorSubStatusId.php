<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject;


use Maxipost\DomainEventSourcing\AggregateRootId;

class ContractorSubStatusId extends AggregateRootId
{

}