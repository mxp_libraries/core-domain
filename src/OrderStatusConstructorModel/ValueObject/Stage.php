<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject;

use function array_keys;
use function array_map;

class Stage
{
    public const TYPES = [
        1 => [
            'id' => 1,
            'name' => 'интернет магазин',
        ],
        2 => [
            'id' => 2,
            'name' => 'забор/магистраль',
        ],
        3 => [
            'id' => 3,
            'name' => 'склад',
        ],
        4 => [
            'id' => 4,
            'name' => 'последняя миля',
        ],
        5 => [
            'id' => 5,
            'name' => 'возврат',
        ],
        6 => [
            'id' => 6,
            'name' => 'технические статусы',
        ],
    ];

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id]['name'];
    }

    public static function getTypeIds(): array
    {
        return array_map(static function ($id) {
            return (string)$id;
        }, array_keys(self::TYPES));
    }
}