<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject;

use function array_keys;
use function array_map;

class SubStatus
{
    public const TYPES = [
        0 => [
            'id' => 0,
            'name' => 'default',
        ],
        201 => [
            'id' => 201,
            'name' => 'недозвон',
        ],
        202 => [
            'id' => 202,
            'name' => 'неверный адрес',
        ],
        203 => [
            'id' => 203,
            'name' => 'нет денег у получателя при вручении',
        ],
        204 => [
            'id' => 204,
            'name' => 'нет телефона получателя или телефон неверен',
        ],
        205 => [
            'id' => 205,
            'name' => 'отказ при вручении из-за товара',
        ],
        206 => [
            'id' => 206,
            'name' => 'отказ от заказа при прозвоне',
        ],
        207 => [
            'id' => 207,
            'name' => 'отказ от заказа при вручении',
        ],
        208 => [
            'id' => 208,
            'name' => 'по просьбе получателя',
        ],
        209 => [
            'id' => 209,
            'name' => 'по просьбе отправителя',
        ],
        210 => [
            'id' => 210,
            'name' => 'изменение адреса',
        ],
        211 => [
            'id' => 211,
            'name' => 'получатель умер',
        ],
        251 => [
            'id' => 251,
            'name' => 'вне зоны доставки',
        ],
        252 => [
            'id' => 252,
            'name' => 'истек срок хранения',
        ],
        253 => [
            'id' => 253,
            'name' => 'нестандартные габариты',
        ],
        999 => [
            'id' => 999,
            'name' => 'причина неизвестна',
        ],
    ];

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id]['name'];
    }

    public static function getTypeIds(): array
    {
        return array_map(static function ($id) {
            return (string)$id;
        }, array_keys(self::TYPES));
    }
}