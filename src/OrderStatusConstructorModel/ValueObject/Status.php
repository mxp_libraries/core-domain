<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject;

use function array_keys;
use function array_map;

class Status
{
    public const TYPES = [
        1 => [
            'id' => 1,
            'name' => 'черновик',
        ],
        2 => [
            'id' => 2,
            'name' => 'подтвержден',
        ],
        3 => [
            'id' => 3,
            'name' => 'в обработке',
        ],
        4 => [
            'id' => 4,
            'name' => 'обработан',
        ],
        5 => [
            'id' => 5,
            'name' => 'передан в службу доставки',
        ],
        6 => [
            'id' => 6,
            'name' => 'принят для транспортировки',
        ],
        7 => [
            'id' => 7,
            'name' => 'передан на склад',
        ],
        8 => [
            'id' => 8,
            'name' => 'принят на склад',
        ],
        9 => [
            'id' => 9,
            'name' => 'сортировка',
        ],
        10 => [
            'id' => 10,
            'name' => 'отгружен со склада',
        ],
        11 => [
            'id' => 11,
            'name' => 'запрошен возврат',
        ],
        12 => [
            'id' => 12,
            'name' => 'передан на пвз',
        ],
        13 => [
            'id' => 13,
            'name' => 'принят на пвз',
        ],
        14 => [
            'id' => 14,
            'name' => 'продлен срок хранения',
        ],
        15 => [
            'id' => 15,
            'name' => 'выдан на доставку',
        ],
        16 => [
            'id' => 16,
            'name' => 'курьер направился к получателю',
        ],
        17 => [
            'id' => 17,
            'name' => 'вручен',
        ],
        18 => [
            'id' => 18,
            'name' => 'перенос даты доставки',
        ],
        19 => [
            'id' => 19,
            'name' => 'вручен частично',
        ],
        20 => [
            'id' => 20,
            'name' => 'оформлен возврат',
        ],
        21 => [
            'id' => 21,
            'name' => 'возврат принят на склад',
        ],
        22 => [
            'id' => 22,
            'name' => 'возврат принят на склад частично',
        ],
        23 => [
            'id' => 23,
            'name' => 'сортировка возврата',
        ],
        24 => [
            'id' => 24,
            'name' => 'возврат отгружен со склада',
        ],
        25 => [
            'id' => 25,
            'name' => 'возврат принят для транспортировки',
        ],
        26 => [
            'id' => 26,
            'name' => 'возврат передан на склад',
        ],
        27 => [
            'id' => 27,
            'name' => 'возврат выдан на доставку',
        ],
        28 => [
            'id' => 28,
            'name' => 'возврат вручен',
        ],
        29 => [
            'id' => 29,
            'name' => 'отмена',
        ],
        30 => [
            'id' => 30,
            'name' => 'утерян',
        ],
        31 => [
            'id' => 31,
            'name' => 'курьер на месте',
        ],
        91 => [
            'id' => 91,
            'name' => 'неизвестный статус',
        ],
        92 => [
            'id' => 92,
            'name' => 'ошибка интеграции',
        ],
    ];

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return self::TYPES[$this->id]['name'];
    }

    public static function getTypeIds(): array
    {
        return array_map(static function ($id) {
            return (string)$id;
        }, array_keys(self::TYPES));
    }
}