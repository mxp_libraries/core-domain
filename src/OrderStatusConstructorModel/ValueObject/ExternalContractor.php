<?php
declare(strict_types=1);

namespace Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject;


class ExternalContractor
{
    private $contractorId;
    private $contractorStatusId;
    private $contractorSubStatusId;
    private $contractorStatusName;
    private $contractorSubStatusName;
    private $description;

    public function __construct(
        ContractorId $contractorId,
        ContractorStatusId $contractorStatusId,
        ContractorSubStatusId $contractorSubStatusId,
        string $contractorStatusName,
        string $contractorSubStatusName,
        string $description
    )
    {
        $this->contractorId = $contractorId;
        $this->contractorStatusId = $contractorStatusId;
        $this->contractorSubStatusId = $contractorSubStatusId;
        $this->contractorStatusName = $contractorStatusName;
        $this->contractorSubStatusName = $contractorSubStatusName;
        $this->description = $description;
    }

    public function getContractorId(): ContractorId
    {
        return $this->contractorId;
    }

    public function getContractorStatusId(): ContractorStatusId
    {
        return $this->contractorStatusId;
    }

    public function getContractorSubStatusId(): ContractorSubStatusId
    {
        return $this->contractorSubStatusId;
    }

    public function getContractorStatusName(): string
    {
        return $this->contractorStatusName;
    }

    public function getContractorSubStatusName(): string
    {
        return $this->contractorSubStatusName;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}